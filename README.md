# Is it safe out there?

An app to collect world-wide real-time data about COVID-19 incidences, deaths and recoveries.

## Execution

The app is highly decoupled, its only dependency being the Johns Hopkins University's data repository. If you want to run it locally, you might be able to experience real-time collection for yourself, but be aware that all data will  be stored in your memory!
 
### Local execution
 
To start the application locally, please run:
 
```sh
$ ./gradlew bootRun
```
 
These commands open a server in `http://localhost:8080/covid19/api`. The following resources are available for you:
 
- `/manage/*`: actuator endpoints, like `/health`, `/env` etc.;
- `/h2-console`: opens the database console. Please refer to `application.yml` to check the credentials for your local station.
 
 ### Production deployment
 
To deploy the application, please run:
 
````sh
$ ./gradlew build
$ cd build/libs
$ jar -xf isitsafeoutthere-0.0.1-SNAPSHOT.jar
$ java org.springframework.boot.loader.JarLauncher --spring.profiles.active=production --jasypt.encryptor.password=${JASYPT_PASSWORD}
````

### Encrypting/decrypting properties

One can use Jasypt's CLI to encrypt/decrypt your secure properties. You can download it via their [GitHub Repository](https://github.com/jasypt/jasypt). Then, use the latest release's algorithm and generators:
 
```sh
$ ./bin/encrypt.sh input=<decrypted input> password=<password> algorithm=PBEWithHMACSHA512AndAES_256  ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator 
```

To decrypt, just do the inverse operation:

```sh
$ ./bin/decrypt.sh input=<encrypted input> password=<password> algorithm=PBEWithHMACSHA512AndAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator 
```

## Getting Started (from Spring Boot initializer)

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.1.BUILD-SNAPSHOT/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.1.BUILD-SNAPSHOT/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Rest Repositories](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#howto-use-exposing-spring-data-repositories-rest-endpoint)
* [Spring HATEOAS](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#boot-features-spring-hateoas)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/reference/htmlsingle/#production-ready)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing JPA Data with REST](https://spring.io/guides/gs/accessing-data-rest/)
* [Accessing Neo4j Data with REST](https://spring.io/guides/gs/accessing-neo4j-data-rest/)
* [Accessing MongoDB Data with REST](https://spring.io/guides/gs/accessing-mongodb-data-rest/)
* [Building a Hypermedia-Driven RESTful Web Service](https://spring.io/guides/gs/rest-hateoas/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)


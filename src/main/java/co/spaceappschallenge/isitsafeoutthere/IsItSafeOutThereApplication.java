package co.spaceappschallenge.isitsafeoutthere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class IsItSafeOutThereApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsItSafeOutThereApplication.class, args);
	}

}

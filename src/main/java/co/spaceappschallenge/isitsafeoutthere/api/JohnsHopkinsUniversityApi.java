package co.spaceappschallenge.isitsafeoutthere.api;

import co.spaceappschallenge.isitsafeoutthere.api.uri.JohnsHopkinsUniversityUriGenerator;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsCsvMapper;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class JohnsHopkinsUniversityApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(JohnsHopkinsUniversityApi.class);

    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    private RestTemplate restTemplate;

    private Covid19RecordsCsvMapper csvMapper;

    @Autowired
    public JohnsHopkinsUniversityApi(JohnsHopkinsUniversityUriGenerator uriGenerator, RestTemplate restTemplate,
                                     Covid19RecordsCsvMapper csvMapper) {
        this.uriGenerator = uriGenerator;
        this.restTemplate = restTemplate;
        this.csvMapper = csvMapper;
    }

    public GitHubContentFileJsonListResource getContentFileResources() {
        LOGGER.info("Requesting for all Johnsons Hopkins University's collected data.");
        return restTemplate.getForObject(uriGenerator.getContentsPath(), GitHubContentFileJsonListResource.class);
    }

    public List<Covid19RecordCsvResource> getCollectedDataFromUri(String uri) {
        LOGGER.trace("Requesting collected data in uri = {}.", uri);

        String result = restTemplate.getForObject(uri, String.class);

        return csvMapper.readResourceFromCommaSeparatedString(result);
    }
}

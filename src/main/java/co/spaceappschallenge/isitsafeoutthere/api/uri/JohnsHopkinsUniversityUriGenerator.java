package co.spaceappschallenge.isitsafeoutthere.api.uri;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class JohnsHopkinsUniversityUriGenerator implements UriGenerator {

    private static final String REPOS_PATH = "repos";
    private static final String CONTENTS_PATH = "contents";

    private final String scheme;
    private final String host;
    private final int port;
    private final String contextPath;
    private final String userName;
    private final String userRepository;
    private final String targetPath;

    @Autowired
    public JohnsHopkinsUniversityUriGenerator(@Value("${johns-hopkins-university.domain.scheme}") String scheme,
                                              @Value("${johns-hopkins-university.domain.host}") String host,
                                              @Value("${johns-hopkins-university.domain.port}") int port,
                                              @Value("${johns-hopkins-university.domain.contextPath}") String contextPath,
                                              @Value("${johns-hopkins-university.user.name}") String userName,
                                              @Value("${johns-hopkins-university.user.repository}") String userRepository,
                                              @Value("${johns-hopkins-university.tree.targetPath}") String targetPath) {
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.contextPath = contextPath;
        this.userName = userName;
        this.userRepository = userRepository;
        this.targetPath = targetPath;
    }

    public URI getContentsPath() {
        return getBasePath()
                .path(REPOS_PATH)
                .path("/")
                .path(userName)
                .path("/")
                .path(userRepository)
                .path("/")
                .path(CONTENTS_PATH)
                .path("/")
                .path(targetPath)
                .build()
                .toUri();
    }

    public URI getContentsFilePath(String additionalPath) {
        return getBasePath()
                .path(REPOS_PATH)
                .path("/")
                .path(userName)
                .path("/")
                .path(userRepository)
                .path("/")
                .path(CONTENTS_PATH)
                .path("/")
                .path(targetPath)
                .path("/")
                .path(additionalPath)
                .build()
                .toUri();
    }


    @Override
    public String getScheme() {
        return scheme;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public String getContextPath() {
        return contextPath;
    }
}

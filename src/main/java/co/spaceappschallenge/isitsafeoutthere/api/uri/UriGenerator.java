package co.spaceappschallenge.isitsafeoutthere.api.uri;

import org.springframework.web.util.UriComponentsBuilder;

public interface UriGenerator {

    String getScheme();
    String getHost();
    int getPort();
    String getContextPath();


    default UriComponentsBuilder getBasePath() {
        return UriComponentsBuilder.newInstance()
                .scheme(getScheme())
                .host(getHost())
                .port(getPort())
                .path(getContextPath());
    }
}

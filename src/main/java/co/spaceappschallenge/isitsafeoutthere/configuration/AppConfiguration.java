package co.spaceappschallenge.isitsafeoutthere.configuration;

import co.spaceappschallenge.isitsafeoutthere.util.computer.EmptyElementMapper;
import co.spaceappschallenge.isitsafeoutthere.util.computer.IntegerEmptyElementMapper;
import co.spaceappschallenge.isitsafeoutthere.util.computer.StringEmptyElementMapper;
import org.springframework.context.annotation.Bean;

import java.util.List;

import static java.util.Arrays.asList;

public class AppConfiguration {

    @Bean
    public List<EmptyElementMapper> elementComputers(StringEmptyElementMapper stringComputer, IntegerEmptyElementMapper integerComputer) {
        return asList(stringComputer, integerComputer);
    }
}

package co.spaceappschallenge.isitsafeoutthere.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
public class AsyncConfiguration {

    @Bean("covid19RecordsSearchTaskExecutor")
    public Executor covid19RecordsSearchTaskExecutor
            (@Value("${covid19-records-search-async-config.core-pool-size}") int corePoolSize,
             @Value("${covid19-records-search-async-config.max-pool-size}") int maxPoolSize,
             @Value("${covid19-records-search-async-config.queue-capacity}") int queueCapacity,
             @Value("${covid19-records-search-async-config.thread-name-prefix}") String threadNamePrefix) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }

    @Bean("covid19RecordsBatchRunnerExecutor")
    public Executor covid19RecordsBatchRunnerExecutor(
            @Value("${covid19-records-batch-runner-async-config.core-pool-size}") int corePoolSize,
            @Value("${covid19-records-batch-runner-async-config.max-pool-size}") int maxPoolSize,
            @Value("${covid19-records-batch-runner-async-config.queue-capacity}") int queueCapacity,
            @Value("${covid19-records-batch-runner-async-config.thread-name-prefix}") String threadNamePrefix) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }

}

package co.spaceappschallenge.isitsafeoutthere.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@ConfigurationProperties(prefix = "johns-hopkins-university")
public class JohnsHopkinsUniversityDateFormatsConfiguration {

    public Set<String> dateFormats;

    public Set<String> getDateFormats() {
        return dateFormats;
    }

    public void setDateFormats(Set<String> dateFormats) {
        this.dateFormats = dateFormats;
    }
}

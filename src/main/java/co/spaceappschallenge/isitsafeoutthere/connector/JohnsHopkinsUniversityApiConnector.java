package co.spaceappschallenge.isitsafeoutthere.connector;

import co.spaceappschallenge.isitsafeoutthere.api.JohnsHopkinsUniversityApi;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsResourceMapper;
import co.spaceappschallenge.isitsafeoutthere.mapper.GitHubFileResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class JohnsHopkinsUniversityApiConnector {

    private GitHubFileResourceMapper fileMapper;

    private Covid19RecordsResourceMapper recordsMapper;

    private JohnsHopkinsUniversityApi api;

    @Autowired
    public JohnsHopkinsUniversityApiConnector(GitHubFileResourceMapper fileMapper,
                                              Covid19RecordsResourceMapper recordsMapper,
                                              JohnsHopkinsUniversityApi api) {
        this.fileMapper = fileMapper;
        this.recordsMapper = recordsMapper;
        this.api = api;
    }

    public Set<GitHubContentFile> getJohnsHopkinsUniversityRepositoryData() {
        return new HashSet<>(fileMapper.fromResourceList(api.getContentFileResources()));
    }

    public Set<Covid19RecordResource> getCovidCasesIncidenceAcccordingToJohnsHopkins(String fileName, String uri) {
        return new HashSet<>(recordsMapper.fromResourceList(fileName, api.getCollectedDataFromUri(uri)));
    }
}

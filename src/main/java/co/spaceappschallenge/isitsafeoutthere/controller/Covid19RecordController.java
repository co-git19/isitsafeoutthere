package co.spaceappschallenge.isitsafeoutthere.controller;

import co.spaceappschallenge.isitsafeoutthere.controller.link.Covid19RecordResponseResourceLinkBuilder;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordControllerMapper;
import co.spaceappschallenge.isitsafeoutthere.service.Covid19RecordService;
import co.spaceappschallenge.isitsafeoutthere.util.Utf8StringQueryParamResolver;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Optional;

import static java.lang.String.format;

@RestController
public class Covid19RecordController {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19RecordController.class);

    private Covid19RecordService recordService;

    private Covid19RecordControllerMapper mapper;

    private Covid19RecordResponseResourceLinkBuilder linkBuilder;

    private Utf8StringQueryParamResolver paramResolver;

    private static final String RECORD_NOT_FOUND_FORMAT = "No COVID-19 record found for id %d.";

    @Autowired
    public Covid19RecordController(Covid19RecordService recordService,
                                   Covid19RecordControllerMapper mapper,
                                   Covid19RecordResponseResourceLinkBuilder linkBuilder,
                                   Utf8StringQueryParamResolver paramResolver) {
        this.recordService = recordService;
        this.mapper = mapper;
        this.linkBuilder = linkBuilder;
        this.paramResolver = paramResolver;
    }

    @GetMapping(value = "/records/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public EntityModel<?> findOne(@PathVariable Long id) {

        Optional<Covid19Record> result = recordService.findOne(id);

        return result
                .map(mapper::fromEntity)
                .map(linkBuilder::addFindOneLink)
                .orElseThrow(() -> new NoSuchElementException(format(RECORD_NOT_FOUND_FORMAT, id)));
    }

    @GetMapping(value = "/records", produces = MediaType.APPLICATION_JSON_VALUE)
    public PagedModel<?> findAll(@PageableDefault Pageable pageable,
                                 PagedResourcesAssembler<Covid19RecordResponseResource> assembler) {

        LOGGER.info("Request to get a page of COVID-19 collected records with parameters = {}.", pageable);

        Page<Covid19Record> result = recordService.findAll(pageable);

        LOGGER.info("Returning page {}/{} of {}/{} results for request = {},{}.", result.getNumber(),
                result.getTotalPages(), result.getNumberOfElements(), result.getTotalElements(), pageable);

        return linkBuilder.addFindAllLinks(mapper.fromPage(result), assembler);
    }

    @GetMapping(value = "/records", params = "startDate", produces = MediaType.APPLICATION_JSON_VALUE)
    public PagedModel<?> findByStartDate(@PageableDefault Pageable pageable,
                                         @RequestParam(required = false) String country,
                                         @RequestParam(required = false) String provinceState,
                                         @RequestParam(required = false) @DateTimeFormat(iso =
                                                 DateTimeFormat.ISO.DATE) LocalDate startDate,
                                         PagedResourcesAssembler<Covid19RecordResponseResource> assembler) {

        return findByQueryParams(pageable, country, provinceState, startDate, null, assembler);
    }

    @GetMapping(value = "/records", params = "endDate", produces = MediaType.APPLICATION_JSON_VALUE)
    public PagedModel<?> findByEndDate(@PageableDefault  Pageable pageable,
                                       @RequestParam(required = false) String country,
                                       @RequestParam(required = false) String provinceState,
                                       @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                       PagedResourcesAssembler<Covid19RecordResponseResource> assembler) {

        return findByQueryParams(pageable, country, provinceState, null, endDate, assembler);
    }

    @GetMapping(value = "/records", params = {"startDate", "endDate"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public PagedModel<?> findByStartAndEndDate(@PageableDefault Pageable pageable,
                                               @RequestParam(required = false) String country,
                                               @RequestParam(required = false) String provinceState,
                                               @RequestParam(required = false) @DateTimeFormat(iso =
                                                       DateTimeFormat.ISO.DATE) LocalDate startDate,
                                               @RequestParam(required = false) @DateTimeFormat(iso =
                                                       DateTimeFormat.ISO.DATE) LocalDate endDate,
                                               PagedResourcesAssembler<Covid19RecordResponseResource> assembler) {

        return findByQueryParams(pageable, country, provinceState, startDate, endDate, assembler);
    }

    private PagedModel<?> findByQueryParams(Pageable pageable, String country, String provinceState,
                                            LocalDate startDate, LocalDate endDate,
                                            PagedResourcesAssembler<Covid19RecordResponseResource> assembler) {

        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParams(paramResolver.doDecode(country),
                paramResolver.doDecode(provinceState), startDate, endDate);

        LOGGER.info("Request to get a page of COVID-19 collected records with parameters = {}/{}.", pageable,
                queryParams);

        Page<Covid19Record> result = recordService.findByQueryParams(pageable, queryParams);

        LOGGER.info("Returning page {}/{} of {}/{} results for request = {},{}.", result.getNumber(),
                result.getTotalPages(), result.getNumberOfElements(), result.getTotalElements(), pageable);

        return linkBuilder.addQueryParamsLinks(mapper.fromPage(result), assembler, pageable, queryParams);
    }

}

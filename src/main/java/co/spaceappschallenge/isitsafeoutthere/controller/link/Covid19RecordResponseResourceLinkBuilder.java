package co.spaceappschallenge.isitsafeoutthere.controller.link;

import co.spaceappschallenge.isitsafeoutthere.controller.Covid19RecordController;
import co.spaceappschallenge.isitsafeoutthere.controller.link.assembler.Covid19RecordResponseResourceAssembler;
import co.spaceappschallenge.isitsafeoutthere.util.Utf8StringQueryParamResolver;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class Covid19RecordResponseResourceLinkBuilder {

    private Covid19RecordResponseResourceAssembler assembler;

    private HateoasPageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private Utf8StringQueryParamResolver paramResolver;

    @Autowired
    public Covid19RecordResponseResourceLinkBuilder(
            Covid19RecordResponseResourceAssembler assembler,
            HateoasPageableHandlerMethodArgumentResolver pageableArgumentResolver,
            Utf8StringQueryParamResolver paramResolver) {
        this.assembler = assembler;
        this.pageableArgumentResolver = pageableArgumentResolver;
        this.paramResolver = paramResolver;
    }

    public EntityModel<?> addFindOneLink(Covid19RecordResponseResource entity) {
        return assembler.toModel(entity);
    }

    public PagedModel<?> addFindAllLinks(Page<Covid19RecordResponseResource> response,
                                         PagedResourcesAssembler<Covid19RecordResponseResource> pageAssembler) {

        return pageAssembler.toModel(response, assembler);
    }

    public PagedModel<?> addQueryParamsLinks(Page<Covid19RecordResponseResource> response,
                                             PagedResourcesAssembler<Covid19RecordResponseResource> pageAssembler,
                                             Pageable pageable, Covid19RecordQueryParams queryParams) {

        UriComponentsBuilder builder = linkTo(methodOn(Covid19RecordController.class).findByStartAndEndDate(pageable,
                paramResolver.doEncode(queryParams.getCountry()), paramResolver.doEncode(queryParams.getProvinceState()),
                queryParams.getStartDate(), queryParams.getEndDate(), pageAssembler)).toUriComponentsBuilder();

        pageableArgumentResolver.enhance(builder, null, pageable);

        return pageAssembler.toModel(response, assembler, Link.of(builder.build().toString()).withSelfRel());
    }
}

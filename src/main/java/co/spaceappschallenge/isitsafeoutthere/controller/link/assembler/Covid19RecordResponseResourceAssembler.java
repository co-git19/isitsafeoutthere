package co.spaceappschallenge.isitsafeoutthere.controller.link.assembler;

import co.spaceappschallenge.isitsafeoutthere.controller.Covid19RecordController;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class Covid19RecordResponseResourceAssembler implements SimpleRepresentationModelAssembler<Covid19RecordResponseResource> {

    private HateoasPageableHandlerMethodArgumentResolver argumentResolver;

    @Autowired
    public Covid19RecordResponseResourceAssembler(HateoasPageableHandlerMethodArgumentResolver argumentResolver) {
        this.argumentResolver = argumentResolver;
    }

    @Override
    public void addLinks(EntityModel<Covid19RecordResponseResource> resource) {
        Long id = Optional.ofNullable(resource.getContent())
                .map(Covid19RecordResponseResource::getId)
                .orElse(null);
        resource.add(linkTo(methodOn(Covid19RecordController.class).findOne(id)).withSelfRel());
    }

    @Override
    public void addLinks(CollectionModel<EntityModel<Covid19RecordResponseResource>> resources) {
        resources.forEach(this::addLinks);
    }

}

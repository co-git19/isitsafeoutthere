package co.spaceappschallenge.isitsafeoutthere.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Covid19Record {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String country;

    @Column
    private String provinceState;

    @Column
    private String region;

    @Column(nullable = false)
    private LocalDate referenceDate;

    @Column
    private Integer confirmed;

    @Column
    private Integer recovered;

    @Column
    private Integer deaths;

    @Column(nullable = false)
    private String source;

    @Column(nullable = false)
    private LocalDateTime createdOn;

    @Column(nullable = false)
    private LocalDateTime updatedOn;

    private Covid19Record() {

    }

    public Covid19Record(Covid19Record other) {
        this(other.country, other.provinceState, other.region, other.referenceDate, other.confirmed, other.recovered,
                other.deaths, other.source);
        createdOn = other.createdOn;
        updatedOn = other.updatedOn;
    }

    public Covid19Record(String country, String provinceState, String region, LocalDate referenceDate,
                         Integer confirmed, Integer recovered, Integer deaths, String source) {
        this.country = country;
        this.provinceState = provinceState;
        this.region = region;
        this.referenceDate = referenceDate;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
        this.source = source;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvinceState() {
        return provinceState;
    }

    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public LocalDate getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(LocalDate referenceDate) {
        this.referenceDate = referenceDate;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Covid19Record)) return false;
        Covid19Record record = (Covid19Record) o;
        return Objects.equals(country, record.country) &&
                Objects.equals(provinceState, record.provinceState) &&
                Objects.equals(region, record.region) &&
                Objects.equals(referenceDate, record.referenceDate) &&
                Objects.equals(confirmed, record.confirmed) &&
                Objects.equals(recovered, record.recovered) &&
                Objects.equals(deaths, record.deaths) &&
                Objects.equals(source, record.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, provinceState, region, referenceDate, confirmed, recovered, deaths, source);
    }

    @Override
    public String toString() {
        return "Covid19Record{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", region='" + region + '\'' +
                ", referenceDate=" + referenceDate +
                ", confirmed=" + confirmed +
                ", recovered=" + recovered +
                ", deaths=" + deaths +
                ", source='" + source + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

package co.spaceappschallenge.isitsafeoutthere.domain.exception;

public class Covid19RecordConversionException extends RuntimeException {

    public Covid19RecordConversionException(Throwable cause) {
        super("There was a problem when converting the data from Johns Hopkins Universtiy. Please refresh your " +
                "browser and try again.", cause);
    }
}

package co.spaceappschallenge.isitsafeoutthere.domain.exception;

import java.util.Set;

public class Covid19RecordConversionParseDateException extends RuntimeException {

    private final String fileName;

    private final Set<String> dateFormats;

    public Covid19RecordConversionParseDateException(String fileName, Set<String> dateFormats) {
        super("Could not convert date " + fileName + " with any of the available formats = " + dateFormats);
        this.fileName = fileName;
        this.dateFormats = dateFormats;
    }

    public String getFileName() {
        return fileName;
    }

    public Set<String> getDateFormats() {
        return dateFormats;
    }
}

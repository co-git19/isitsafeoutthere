package co.spaceappschallenge.isitsafeoutthere.domain.exception;

import static java.lang.String.format;

public class EmptyElementMapperNotFoundException extends RuntimeException {

    public EmptyElementMapperNotFoundException(Class<?> clazz) {
        super(format("Could not find an appropriate computer for class %s.", clazz));
    }
}

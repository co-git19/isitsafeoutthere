package co.spaceappschallenge.isitsafeoutthere.domain.exception;

import java.io.UnsupportedEncodingException;

import static java.lang.String.format;

public class QueryParamDecodeException extends RuntimeException {

    public QueryParamDecodeException(String param, UnsupportedEncodingException unsupportedEncodingException) {
        super(getMessage(param, unsupportedEncodingException), unsupportedEncodingException);
    }

    public QueryParamDecodeException(String param, Throwable throwable) {
        super(getMessage(param, throwable), throwable);
    }

    private static String getMessage(String param, Throwable cause) {
        return format("Cannot decode query parameter %s. Reason: %s.", param, cause.getMessage());
    }
}

package co.spaceappschallenge.isitsafeoutthere.domain.exception;

import java.io.UnsupportedEncodingException;

import static java.lang.String.format;

public class QueryParamEncodeException extends RuntimeException {

    public QueryParamEncodeException(String param, UnsupportedEncodingException unsupportedEncodingException) {
        super(getMessage(param, unsupportedEncodingException), unsupportedEncodingException);
    }

    public QueryParamEncodeException(String param, Throwable throwable) {
        super(getMessage(param, throwable), throwable);
    }

    private static String getMessage(String param, Throwable cause) {
        return format("Cannot encode query parameter %s. Reason: %s.", param, cause.getMessage());
    }
}

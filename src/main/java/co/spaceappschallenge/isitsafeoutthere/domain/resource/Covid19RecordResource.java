package co.spaceappschallenge.isitsafeoutthere.domain.resource;

import java.time.LocalDate;
import java.util.Objects;

public class Covid19RecordResource {

    private String country;

    private String provinceState;

    private String region;

    private LocalDate referenceDate;

    private Integer confirmed;

    private Integer recovered;

    private Integer deaths;

    private String source;

    public Covid19RecordResource(String country, String provinceState, String region, LocalDate referenceDate,
                                 Integer confirmed, Integer recovered, Integer deaths, String source) {
        this.country = country;
        this.provinceState = provinceState;
        this.region = region;
        this.referenceDate = referenceDate;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
        this.source = source;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvinceState() {
        return provinceState;
    }

    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public LocalDate getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(LocalDate referenceDate) {
        this.referenceDate = referenceDate;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Covid19RecordResource)) return false;
        Covid19RecordResource that = (Covid19RecordResource) o;
        return Objects.equals(country, that.country) &&
                Objects.equals(provinceState, that.provinceState) &&
                Objects.equals(region, that.region) &&
                Objects.equals(referenceDate, that.referenceDate) &&
                Objects.equals(confirmed, that.confirmed) &&
                Objects.equals(recovered, that.recovered) &&
                Objects.equals(deaths, that.deaths) &&
                Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, provinceState, region, referenceDate, confirmed, recovered, deaths, source);
    }


    @Override
    public String toString() {
        return "Covid19RecordResource{" +
                "country='" + country + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", region='" + region + '\'' +
                ", referenceDate=" + referenceDate +
                ", confirmed=" + confirmed +
                ", recovered=" + recovered +
                ", deaths=" + deaths +
                ", source='" + source + '\'' +
                '}';
    }
}

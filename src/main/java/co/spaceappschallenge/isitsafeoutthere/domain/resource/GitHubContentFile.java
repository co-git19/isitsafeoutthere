package co.spaceappschallenge.isitsafeoutthere.domain.resource;

import java.util.Objects;

public class GitHubContentFile {

    private String name;

    private String downloadUrl;

    public GitHubContentFile(String name, String downloadUrl) {
        this.name = name;
        this.downloadUrl = downloadUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GitHubContentFile)) return false;
        GitHubContentFile that = (GitHubContentFile) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(downloadUrl, that.downloadUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, downloadUrl);
    }

    @Override
    public String toString() {
        return "GitHubContentFile{" +
                "name='" + name + '\'' +
                ", downloadUrl='" + downloadUrl + '\'' +
                '}';
    }
}

package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class Covid19RecordControllerMapper {

    public Page<Covid19RecordResponseResource> fromPage(Page<Covid19Record> entities) {
        return entities.map(this::fromEntity);
    }

    public Covid19RecordResponseResource fromEntity(Covid19Record entity) {
        return new Covid19RecordResponseResource(entity);
    }
}

package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import org.springframework.stereotype.Component;

@Component
public class Covid19RecordUpdater {

    public boolean shouldUpdate(Covid19Record currentState, Covid19Record newState) {
        return !currentState.equals(newState);
    }

    public Covid19Record doUpdate(Covid19Record currentState, Covid19Record newState) {
        Covid19Record copy = new Covid19Record(currentState);
        copy.setConfirmed(newState.getConfirmed());
        copy.setRecovered(newState.getRecovered());
        copy.setDeaths(newState.getDeaths());
        copy.setSource(newState.getSource());
        return copy;
    }
}

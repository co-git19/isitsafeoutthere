package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.exception.Covid19RecordConversionException;
import co.spaceappschallenge.isitsafeoutthere.util.CsvBasicHeaderConverter;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import com.fasterxml.jackson.databind.MappingIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.fasterxml.jackson.databind.MappingIterator.emptyIterator;

@Component
public class Covid19RecordsCsvMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19RecordsCsvMapper.class);

    private CsvBasicHeaderConverter csvBasicHeaderConverter;

    @Autowired
    public Covid19RecordsCsvMapper(
            CsvBasicHeaderConverter csvBasicHeaderConverter) {
        this.csvBasicHeaderConverter = csvBasicHeaderConverter;
    }

    public List<Covid19RecordCsvResource> readResourceFromCommaSeparatedString(String data) {

        MappingIterator<Covid19RecordCsvResource> resultIterator = Optional.ofNullable(data)
                    .map(this::mapRecord)
                    .orElse(emptyIterator());

        List<Covid19RecordCsvResource> result = new ArrayList<>();
        while(resultIterator.hasNext()) {
            result.add(resultIterator.next());
        }

        return result;
    }

    private MappingIterator<Covid19RecordCsvResource> mapRecord(String data) {
        try {
            return csvBasicHeaderConverter.getForClass(Covid19RecordCsvResource.class).readValues(data);
        } catch (IOException io) {
            throw new Covid19RecordConversionException(io);
        }

    }

}

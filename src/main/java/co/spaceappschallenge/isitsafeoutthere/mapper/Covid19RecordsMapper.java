package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import org.springframework.stereotype.Component;

import java.util.List;
import static java.util.stream.Collectors.toList;

@Component
public class Covid19RecordsMapper {

    public List<Covid19Record> fromResourcesList(List<Covid19RecordResource> resourceList) {
        return resourceList
                .stream()
                .map(this::fromResource)
                .collect(toList());
    }

    public Covid19Record fromResource(Covid19RecordResource resource) {
        return new Covid19Record(resource.getCountry(), resource.getProvinceState(),
                resource.getRegion(), resource.getReferenceDate(), resource.getConfirmed(), resource.getRecovered(),
                resource.getDeaths(), resource.getSource());
    }
}

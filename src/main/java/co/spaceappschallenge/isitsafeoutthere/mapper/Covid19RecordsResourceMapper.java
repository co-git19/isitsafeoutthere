package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.api.uri.JohnsHopkinsUniversityUriGenerator;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.util.ValueUtility;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class Covid19RecordsResourceMapper {

    private GitHubDateFromFileNameMapper fileNameMapper;

    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    private ValueUtility utility;

    @Autowired
    public Covid19RecordsResourceMapper(GitHubDateFromFileNameMapper fileNameMapper,
                                        JohnsHopkinsUniversityUriGenerator uriGenerator,
                                        ValueUtility utility) {
        this.fileNameMapper = fileNameMapper;
        this.uriGenerator = uriGenerator;
        this.utility = utility;
    }

    public List<Covid19RecordResource> fromResourceList(String referenceFileName,
                                                        List<Covid19RecordCsvResource> resourcesList) {
        final LocalDate referenceDate = fileNameMapper.fromFileName(referenceFileName);
        return resourcesList
                .stream()
                .map(resource -> doConversion(referenceDate, referenceFileName, resource))
                .collect(toList());
    }

    public Covid19RecordResource fromResource(String referenceFileName, Covid19RecordCsvResource resource) {
        final LocalDate referenceDate = fileNameMapper.fromFileName(referenceFileName);
        return doConversion(referenceDate, referenceFileName, resource);
    }

    private Covid19RecordResource doConversion(LocalDate referenceDate,
                                               String originalFileName, Covid19RecordCsvResource resource) {
        final String countryRegion = utility.computeIfNull(resource.currentCountryRegion,
                utility.emptyIfNull(resource.legacyCountryRegion, String.class));
        final String provinceState = utility.computeIfNull(resource.currentProvinceState,
                utility.emptyIfNull(resource.legacyProvinceState, String.class));
        final String region = utility.emptyIfNull(resource.region, String.class);
        return new Covid19RecordResource(countryRegion, provinceState, region, referenceDate, resource.confirmed,
                resource.recovered, resource.deaths, uriGenerator.getContentsFilePath(originalFileName).toString());
    }
}

package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.configuration.JohnsHopkinsUniversityDateFormatsConfiguration;
import co.spaceappschallenge.isitsafeoutthere.domain.exception.Covid19RecordConversionParseDateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.Set;

import static java.time.LocalDate.parse;

@Component
public class GitHubDateFromFileNameMapper {

    private final Set<String> dateFormats;

    @Autowired
    public GitHubDateFromFileNameMapper(JohnsHopkinsUniversityDateFormatsConfiguration dateFormatsConfiguration) {
        this.dateFormats = dateFormatsConfiguration.getDateFormats();
    }

    public LocalDate fromFileName(String fileName) {
        final String referenceDate = fileName.substring(0, fileName.lastIndexOf('.'));
        return dateFormats
               .stream()
               .map(format -> convertIfPossible(referenceDate, format))
               .filter(Objects::nonNull)
               .findFirst()
               .orElseThrow(() -> new Covid19RecordConversionParseDateException(fileName, dateFormats));
    }

    private LocalDate convertIfPossible(String fileName, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        try {
            return parse(fileName, formatter);
        } catch (DateTimeParseException dte) {
            return null;
        }
    }
}

package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonResource;
import org.springframework.stereotype.Component;

import java.util.List;
import static java.util.stream.Collectors.toList;

@Component
public class GitHubFileResourceMapper {

    public List<GitHubContentFile> fromResourceList(GitHubContentFileJsonListResource listResource) {
        return listResource
                .stream()
                .map(this::fromResource)
                .collect(toList());
    }

    public GitHubContentFile fromResource(GitHubContentFileJsonResource resource) {
        return new GitHubContentFile(resource.name, resource.downloadUrl);
    }



}

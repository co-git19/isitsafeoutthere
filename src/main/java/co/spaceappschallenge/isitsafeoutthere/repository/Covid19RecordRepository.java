package co.spaceappschallenge.isitsafeoutthere.repository;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.repository.custom.Covid19RecordRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Set;

@Repository
public interface Covid19RecordRepository extends JpaRepository<Covid19Record, Long>, Covid19RecordRepositoryCustom {

    Page<Covid19Record> findByCountryAndProvinceState(String country, String provinceState, Pageable pageable);

    Page<Covid19Record> findByCountryInAndProvinceStateInAndReferenceDateIn(Set<String> countries,
                                                                            Set<String> provincesSates,
                                                                            Set<LocalDate> referenceDates,
                                                                            Pageable pageable);
}

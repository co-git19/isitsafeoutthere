package co.spaceappschallenge.isitsafeoutthere.repository.custom;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface Covid19RecordRepositoryCustom {

    Page<Covid19Record> findByQueryParameters(Pageable pageable, Covid19RecordQueryParams queryParams);
}

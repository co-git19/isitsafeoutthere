package co.spaceappschallenge.isitsafeoutthere.repository.custom;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.util.PageableRepositoryUtils;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.util.Assert.isTrue;

@Repository
public class Covid19RecordRepositoryImpl implements Covid19RecordRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    private PageableRepositoryUtils repositoryUtils;

    @Autowired
    public Covid19RecordRepositoryImpl(PageableRepositoryUtils repositoryUtils) {
        this.repositoryUtils = repositoryUtils;
    }

    @Transactional
    @Override
    public Page<Covid19Record> findByQueryParameters(Pageable pageable, Covid19RecordQueryParams queryParams) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Covid19Record> query = builder.createQuery(Covid19Record.class);

        Root<Covid19Record> root = query.from(Covid19Record.class);
        query.where(createQueryAndPredicate(builder, root, queryParams));
        query.orderBy(repositoryUtils.toOrders(pageable, root));

        TypedQuery<Covid19Record> resultQuery = entityManager.createQuery(query);
        resultQuery.setFirstResult((int) pageable.getOffset());
        resultQuery.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(resultQuery.getResultList(), pageable, getTotalCount(queryParams));
    }

    private long getTotalCount(Covid19RecordQueryParams queryParams) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Covid19Record> root = query.from(Covid19Record.class);

        query.select(builder.count(root.get("id")));
        query.where(createQueryAndPredicate(builder, root, queryParams));

        return entityManager.createQuery(query).getSingleResult();
    }

    private Predicate createQueryAndPredicate(CriteriaBuilder builder, Root<Covid19Record> root,
                                              Covid19RecordQueryParams queryParams) {

        final Optional<String> country = Optional.ofNullable(queryParams.getCountry());
        final Optional<String> provinceState = Optional.ofNullable(queryParams.getProvinceState());
        final Optional<LocalDate> startDate = Optional.ofNullable(queryParams.getStartDate());
        final Optional<LocalDate> endDate = Optional.ofNullable(queryParams.getEndDate());

        if (startDate.isPresent() && endDate.isPresent()) {
            assertDateIntervals(startDate.get(), endDate.get());
        }

        final List<Predicate> predicates = new ArrayList<>();
        country.ifPresent(it -> predicates.add(builder.equal(root.get("country"), it)));
        provinceState.ifPresent(it -> predicates.add(builder.equal(root.get("provinceState"), it)));
        startDate.ifPresent(it -> predicates.add(builder.greaterThanOrEqualTo(root.get("referenceDate"), it)));
        endDate.ifPresent(it -> predicates.add(builder.lessThanOrEqualTo(root.get("referenceDate"), it)));

        return builder.and(predicates.toArray(new Predicate[]{}));
    }

    private void assertDateIntervals(LocalDate startDate, LocalDate endDate) {
        isTrue(endDate.isAfter(startDate) || endDate.isEqual(startDate), format("End date (%s) should not be before " +
                        "start date (%s)", endDate, startDate));
    }
}

package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.util.AsyncResultsResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Lists.partition;
import static org.springframework.util.CollectionUtils.isEmpty;

@Component
public class Covid19DataCrawlerBatchCreationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19DataCrawlerBatchCreationService.class);

    private Covid19ResourceService universityService;

    private Covid19DataCrawlerBatchPreparationService batchPreparator;

    private AsyncResultsResolver asyncResultsResolver;

    private final int batchSize;

    @Autowired
    public Covid19DataCrawlerBatchCreationService(
            Covid19ResourceService resourceService,
            Covid19DataCrawlerBatchPreparationService batchPreparator,
            AsyncResultsResolver asyncResultsResolver,
            @Value("${covid19-records-batch-runner-async-config.batch-size}") int batchSize) {
        this.universityService = resourceService;
        this.batchPreparator = batchPreparator;
        this.asyncResultsResolver = asyncResultsResolver;
        this.batchSize = batchSize;
    }

    public void createAndRunBatches() throws InterruptedException, ExecutionException {
        List<Covid19RecordResource> records = getAllRecords();

        if (isEmpty(records)) {
            LOGGER.warn("No records were raised. Therefore, aborting the batch creation.");
            return;
        }

        LOGGER.info("Preparing to split all {} records into chunks of size = {}.", records.size(), batchSize);

        List<List<Covid19RecordResource>> chunks = partition(records, batchSize);

        LOGGER.info("Finished splitting all {} records into chunks of size = {}. There were identified {} chunks.",
                records.size(), batchSize, chunks.size());

        List<CompletableFuture<List<Covid19Record>>> asyncResults = new ArrayList<>();
        for (int partitionIndex = 0; partitionIndex < chunks.size(); partitionIndex++) {
            asyncResults.add(runPartition(chunks.get(partitionIndex), partitionIndex, chunks.size()));
        }

        List<List<Covid19Record>> results = asyncResultsResolver.joinResults(asyncResults);

        LOGGER.info("All {} batches of size {} finished successfully. Processed {} records.", results.size(),
                batchSize, records.size());

    }

    private List<Covid19RecordResource> getAllRecords() {
        LOGGER.info("Preparing to fetch and order all COVID-19 records");

        List<Covid19RecordResource> covid19Records = new ArrayList<>(universityService.getAllCovid19Records());

        LOGGER.info("Finished data ordering for {} records.", covid19Records.size());
        return covid19Records;
    }

    private CompletableFuture<List<Covid19Record>> runPartition(
            List<Covid19RecordResource> partition, int partitionIndex, int total) {

        LOGGER.info("Running partition = {}/{} for batch with size = {}.", ++partitionIndex, total, partition.size());
        return batchPreparator.prepareAndRun(partitionIndex, total, partition);
    }
}

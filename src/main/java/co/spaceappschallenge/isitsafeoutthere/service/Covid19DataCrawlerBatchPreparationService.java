package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsMapper;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordFieldsExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.completedFuture;

@Component
public class Covid19DataCrawlerBatchPreparationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19DataCrawlerBatchPreparationService.class);

    private Covid19DataCrawlerBatchRunnerService batchRunner;

    private Covid19RecordFieldsExtractor fieldsExtractor;

    private Covid19RecordSortService sortService;

    private Covid19RecordsMapper mapper;

    private Covid19RecordService service;

    @Autowired
    public Covid19DataCrawlerBatchPreparationService(
            Covid19DataCrawlerBatchRunnerService batchRunner,
            Covid19RecordFieldsExtractor fieldsExtractor,
            Covid19RecordSortService sortService,
            Covid19RecordsMapper mapper,
            Covid19RecordService service) {
        this.batchRunner = batchRunner;
        this.fieldsExtractor = fieldsExtractor;
        this.sortService = sortService;
        this.mapper = mapper;
        this.service = service;
    }

    @Async("covid19RecordsBatchRunnerExecutor")
    public CompletableFuture<List<Covid19Record>> prepareAndRun(int batchId, int totalBatches,
                                                                List<Covid19RecordResource> batch) {

        LOGGER.info("Preparing to run batch {}/{} with {} elements.", batchId, totalBatches, batch.size());

        List<Covid19Record> matchingElements = findMatchingElements(batch);
        List<Covid19Record> convertedBatch = convertBatchToRecords(batch);

        matchingElements = sortService.sort(matchingElements);
        convertedBatch = sortService.sort(convertedBatch);

        List<Covid19Record> result = runBatch(matchingElements, convertedBatch);
        result = service.save(result);

        LOGGER.info("Finished execution of batch {}/{} with {} elements.", batchId, totalBatches, batch.size());

        return completedFuture(result);
    }

    private List<Covid19Record> findMatchingElements(List<Covid19RecordResource> batch) {
        return service.findByCountriesAndStateProvinciesAndReferenceDates(fieldsExtractor.extractCountries(batch),
                fieldsExtractor.extractStatesProvinces(batch), fieldsExtractor.extractReferenceDates(batch),
                PageRequest.of(0, batch.size())).toList();
    }

    private List<Covid19Record> convertBatchToRecords(List<Covid19RecordResource> batch) {
        return mapper.fromResourcesList(batch);
    }

    private List<Covid19Record> runBatch(List<Covid19Record> matchingElements, List<Covid19Record> batch) {
        return batchRunner.runBatch(matchingElements, batch);
    }
}

package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordUpdater;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;

import static java.util.Collections.binarySearch;

@Component
public class Covid19DataCrawlerBatchRunnerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19DataCrawlerBatchRunnerService.class);

    private Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator;


    private Covid19RecordUpdater updater;

    @Autowired
    public Covid19DataCrawlerBatchRunnerService(
            Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator,
            Covid19RecordUpdater updater) {
        this.comparator = comparator;
        this.updater = updater;
    }

    public List<Covid19Record> runBatch(List<Covid19Record> currentRecords, List<Covid19Record> batch) {
        LOGGER.debug("Preparing to run batch with {} elements against current {} existing records.", batch.size(),
                currentRecords.size());

        List<Covid19Record> result = executeBatch(currentRecords, batch);

        LOGGER.debug("Finished processing batch of {} elements against current {} raised records. Gonna return {} " +
                        "elements.", batch.size(), currentRecords.size(), result.size());

        return result;
    }

    private List<Covid19Record> executeBatch(List<Covid19Record> currentRecords, List<Covid19Record> recordsBatch) {
        return recordsBatch
                .stream()
                .map(batchRecord -> mapBatchRecordIfApplicable(currentRecords, batchRecord))
                .collect(toList());
    }

    private Covid19Record mapBatchRecordIfApplicable(List<Covid19Record> currentRecords,
                                                     Covid19Record batchRecord) {

        return findMatchingRecord(currentRecords, batchRecord)
                .filter(it -> updater.shouldUpdate(it, batchRecord))
                .map(it -> updater.doUpdate(it, batchRecord))
                .orElse(batchRecord);
    }

    private Optional<Covid19Record> findMatchingRecord(List<Covid19Record> currentRecords,
                                                       Covid19Record batchRecord) {

        Integer index = binarySearch(currentRecords, batchRecord, comparator);

        return Optional.of(index)
                .filter(returnedIndex -> returnedIndex >= 0 && returnedIndex < currentRecords.size())
                .map(currentRecords::get);
    }

}

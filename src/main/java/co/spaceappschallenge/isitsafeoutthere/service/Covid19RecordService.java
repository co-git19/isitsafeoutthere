package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.repository.Covid19RecordRepository;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.time.Clock.systemUTC;
import static java.time.LocalDateTime.now;

@Service
public class Covid19RecordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(Covid19RecordService.class);

    private Covid19RecordRepository repository;

    @Autowired
    public Covid19RecordService(Covid19RecordRepository repository) {
        this.repository = repository;
    }

    public <C extends Collection<Covid19Record>> List<Covid19Record> save(C records) {
        LOGGER.info("Request to save {} entities.", records.size());

        LocalDateTime createTime = now(systemUTC());
        records.forEach(record -> {
                    setCreatedOn(record, createTime);
                    setUpdatedOn(record, createTime);
        });
        List<Covid19Record> result = repository.saveAll(records);

        LOGGER.info("Saved all {} entities successfully.", result.size());
        return result;
    }

    public Optional<Covid19Record> findOne(Long id) {
        LOGGER.trace("Request to find record with id = {}.", id);

        Optional<Covid19Record> record = repository.findById(id);

        if (record.isPresent()) {
            LOGGER.trace("Record was found = {}.", record.get());
        } else {
            LOGGER.trace("No record was found for id = {}.", id);
        }

        return record;
    }

    public Page<Covid19Record> findAll(Pageable pageable) {
        LOGGER.info("Request to find a page of all records with configuration = {}.", pageable);

        Page<Covid19Record> result = repository.findAll(pageable);

        LOGGER.info("Request to find a page of records with configuration = {} raised {}/{} results and {} page(s).",
                pageable, result.getNumberOfElements(), result.getTotalElements(), result.getTotalPages());
        return result;
    }

    public Page<Covid19Record> findByQueryParams(Pageable pageable, Covid19RecordQueryParams queryParams) {
        LOGGER.info("Request to find a page of all records with configuration = {},{}.", pageable, queryParams);

        Page<Covid19Record> result = repository.findByQueryParameters(pageable, queryParams);

        LOGGER.info("Request to find a page of records with configuration = {},{} raised {}/{} results and {} page(s).",
                pageable, queryParams, result.getNumberOfElements(), result.getTotalElements(), result.getTotalPages());
        return result;
    }

    public Page<Covid19Record> findByCountriesAndStateProvinciesAndReferenceDates(Set<String> countries,
                                                                                  Set<String> statesProvinces,
                                                                                  Set<LocalDate> referenceDates,
                                                                                  Pageable pageable) {

        LOGGER.info("Request to find a page with countries {}, {} states/provinces, {} reference dates and " +
                "configuration = {}.", countries.size(), statesProvinces.size(), referenceDates.size(), pageable);

        Page<Covid19Record> result = repository.findByCountryInAndProvinceStateInAndReferenceDateIn(countries,
                statesProvinces, referenceDates, pageable);

        LOGGER.info("Request to find a page with countries {}, {} states/provinces, {} reference dates " +
                "configuration = {} raised {}/{} results and {} page(s).", countries.size(), statesProvinces.size(),
                referenceDates.size(), pageable, result.getNumberOfElements(), result.getTotalElements(),
                result.getTotalPages());

        return result;
    }

    public void delete(Long id) {
        LOGGER.info("Request to delete entity by id = {}.", id);
        repository.deleteById(id);
        LOGGER.info("Entity with id = {} deleted successfully.", id);
    }

    public void delete(Covid19Record record) {
        LOGGER.info("Request to delete entity = {}.", record);
        repository.delete(record);
        LOGGER.info("Entity = {} deleted successfully.", record);
    }

    private void setCreatedOn(Covid19Record current, LocalDateTime createTime) {
        if (Objects.isNull(current.getCreatedOn())) {
            current.setCreatedOn(createTime);
        }
    }

    private void setUpdatedOn(Covid19Record current, LocalDateTime updateTime) {
        current.setUpdatedOn(updateTime);
    }
}

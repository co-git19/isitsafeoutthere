package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Covid19RecordSortService {

    private Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator;

    @Autowired
    public Covid19RecordSortService(Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator) {
        this.comparator = comparator;
    }

    public List<Covid19Record> sort(List<Covid19Record> input) {
        List<Covid19Record> copy = new ArrayList<>(input);
        copy.sort(comparator);
        return copy;
    }
}

package co.spaceappschallenge.isitsafeoutthere.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
@Profile("!test")
public class Covid19RecordsDataCollectionTrigger {

    private Covid19DataCrawlerBatchCreationService batchesCreationService;

    @Autowired
    public Covid19RecordsDataCollectionTrigger(Covid19DataCrawlerBatchCreationService batchesCreationService) {
        this.batchesCreationService = batchesCreationService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void trigger() throws InterruptedException, ExecutionException {
        batchesCreationService.createAndRunBatches();
    }
}

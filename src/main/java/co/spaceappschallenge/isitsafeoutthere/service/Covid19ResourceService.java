package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;

import java.util.Set;

public interface Covid19ResourceService {

    Set<Covid19RecordResource> getAllCovid19Records();
}

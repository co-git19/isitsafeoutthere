package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.connector.JohnsHopkinsUniversityApiConnector;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Component
public class JohnsHopkinsUniversityCollectedDataService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JohnsHopkinsUniversityCollectedDataService.class);

    private JohnsHopkinsUniversityApiConnector connector;

    @Autowired
    public JohnsHopkinsUniversityCollectedDataService(JohnsHopkinsUniversityApiConnector connector) {
        this.connector = connector;
    }

    public Set<GitHubContentFile> getAllCollectedFilesToDownload() {
        LOGGER.info("Requesting for all collected data that needs to be organized and downloaded.");

        Set<GitHubContentFile> result = connector.getJohnsHopkinsUniversityRepositoryData();

        result = result
                .stream()
                .filter(file -> file.getName().endsWith(".csv"))
                .collect(toSet());

        LOGGER.info("Need to download {} files.", result.size());
        LOGGER.trace("Data to be downloaded: {}.", result);

        return result;
    }
}

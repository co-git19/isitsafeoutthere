package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.connector.JohnsHopkinsUniversityApiConnector;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static java.util.Collections.emptySet;
import static java.util.concurrent.CompletableFuture.completedFuture;

@Component
public class JohnsHopkinsUniversityCovid19Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(JohnsHopkinsUniversityCovid19Service.class);

    private JohnsHopkinsUniversityApiConnector connector;

    @Autowired
    public JohnsHopkinsUniversityCovid19Service(JohnsHopkinsUniversityApiConnector connector) {
        this.connector = connector;
    }

    @Async("covid19RecordsSearchTaskExecutor")
    public CompletableFuture<Set<Covid19RecordResource>> getAllSamplesByFileNameAndSets(String fileName, String uri) {
        LOGGER.info("Requesting for all incidences for file = {} and uri = {}.", fileName, uri);

        Set<Covid19RecordResource> records = emptySet();
        try {
            records = connector.getCovidCasesIncidenceAcccordingToJohnsHopkins(fileName, uri);

            LOGGER.info("Found {} records for file = {} and uri = {}.", records.size(), fileName, uri);

        } catch (RuntimeException re) {
            LOGGER.error("There happened an exception for file = {} when downloading {}. Maybe some data issues. " +
                    "Details = {}", fileName, uri, re.getMessage());
        } finally {
            return completedFuture(records);
        }
    }
}

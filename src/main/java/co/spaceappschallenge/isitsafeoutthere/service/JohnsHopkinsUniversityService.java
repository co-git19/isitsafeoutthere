package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.util.AsyncResultsResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class JohnsHopkinsUniversityService implements Covid19ResourceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JohnsHopkinsUniversityService.class);

    private JohnsHopkinsUniversityCollectedDataService collectedDataService;

    private JohnsHopkinsUniversityCovid19Service covid19Service;

    private AsyncResultsResolver resultsResolver;

    @Autowired
    public JohnsHopkinsUniversityService(JohnsHopkinsUniversityCollectedDataService collectedDataService,
                                         JohnsHopkinsUniversityCovid19Service covid19Service,
                                         AsyncResultsResolver resultsResolver) {
        this.collectedDataService = collectedDataService;
        this.covid19Service = covid19Service;
        this.resultsResolver = resultsResolver;
    }

    @Override
    public Set<Covid19RecordResource> getAllCovid19Records() {
        LOGGER.info("Getting request to provide ALL covid-19 information in United States.");

        Set<GitHubContentFile> availableData = collectedDataService.getAllCollectedFilesToDownload();

        List<CompletableFuture<Set<Covid19RecordResource>>> asyncResults = new ArrayList<>();

        for (GitHubContentFile file : availableData) {
            asyncResults.add(covid19Service.getAllSamplesByFileNameAndSets(file.getName(),
                    file.getDownloadUrl()));
        }

        Set<Covid19RecordResource> allResults = completeAllAsyncResults(asyncResults)
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());


        LOGGER.info("The size of the raised dataset is = {}.", allResults.size());

        return allResults;
    }

    private List<Set<Covid19RecordResource>> completeAllAsyncResults(
            List<CompletableFuture<Set<Covid19RecordResource>>> asyncResults) {

        LOGGER.info("Gonna resolve {} async results.", asyncResults.size());

        List<Set<Covid19RecordResource>> results = Collections.emptyList();

        try {
            results = resultsResolver.joinResults(asyncResults);
            LOGGER.info("All {} async results could be completed.", results.size());
        } catch (InterruptedException ie) {
            LOGGER.error("There was an interrupted exception while fetching for the results: {}.", ie);
        } catch (ExecutionException ex) {
            LOGGER.error("There was an execution exception. Details: {}.", ex);
        } finally{
            return results;
        }

    }
}

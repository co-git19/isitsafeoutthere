package co.spaceappschallenge.isitsafeoutthere.util;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.stream.Collectors.toList;

@Component
public class AsyncResultsResolver {

    public <T, C extends Collection<T>> List<C> joinResults(List<CompletableFuture<C>> asyncResults)
            throws InterruptedException, ExecutionException {

        return allOf(asyncResults.toArray(new CompletableFuture[]{}))
                .thenApply(voit -> asyncResults
                        .stream()
                        .map(CompletableFuture::join)
                        .collect(toList()))
                .get();
    }
}

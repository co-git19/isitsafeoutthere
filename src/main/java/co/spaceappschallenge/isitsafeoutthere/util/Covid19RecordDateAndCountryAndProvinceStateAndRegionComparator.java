package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public class Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator implements Comparator<Covid19Record> {

    @Override
    public int compare(Covid19Record first, Covid19Record second) {
        int result = first.getReferenceDate().compareTo(second.getReferenceDate());

        if (result == 0) {
            result = first.getCountry().compareTo(second.getCountry());
        }

        if (result == 0) {
            result = first.getProvinceState().compareTo(second.getProvinceState());
        }

        if (result == 0) {
            result = first.getRegion().compareTo(second.getRegion());
        }

        return result;
    }
}

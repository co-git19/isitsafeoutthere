package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import static java.util.stream.Collectors.toSet;

@Component
public class Covid19RecordFieldsExtractor {

    public Set<String> extractCountries(List<Covid19RecordResource> resources) {
        return resources
                .stream()
                .map(Covid19RecordResource::getCountry)
                .collect(toSet());
    }

    public Set<String> extractStatesProvinces(List<Covid19RecordResource> resources) {
        return resources
                .stream()
                .map(Covid19RecordResource::getProvinceState)
                .collect(toSet());
    }

    public Set<LocalDate> extractReferenceDates(List<Covid19RecordResource> resources) {
        return resources
                .stream()
                .map(Covid19RecordResource::getReferenceDate)
                .collect(toSet());
    }
}

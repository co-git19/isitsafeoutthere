package co.spaceappschallenge.isitsafeoutthere.util;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Component;

@Component
public class CsvBasicHeaderConverter {

    private CsvMapper csvMapper;

    private CsvSchema csvSchema;

    public CsvBasicHeaderConverter() {
        this.csvMapper = new CsvMapper();
        this.csvSchema = csvMapper.schemaWithHeader();
    }

    public ObjectReader getForClass(Class<?> clazz) {
        return csvMapper
                .readerFor(clazz)
                .with(csvSchema);
    }
}

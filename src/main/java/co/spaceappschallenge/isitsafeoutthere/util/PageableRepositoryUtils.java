package co.spaceappschallenge.isitsafeoutthere.util;

import org.hibernate.query.criteria.internal.OrderImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class PageableRepositoryUtils {

    public <Entity> List<Order> toOrders(Pageable pageable, Root<Entity> root) {
        return pageable.getSort()
                .stream()
                .map(it -> new OrderImpl(root.get(it.getProperty()), it.isAscending()))
                .collect(toList());
    }
}

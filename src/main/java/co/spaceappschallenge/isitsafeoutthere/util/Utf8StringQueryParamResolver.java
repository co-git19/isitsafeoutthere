package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.exception.QueryParamEncodeException;
import co.spaceappschallenge.isitsafeoutthere.domain.exception.QueryParamDecodeException;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

import static java.net.URLDecoder.decode;
import static java.net.URLEncoder.encode;

import static org.springframework.util.StringUtils.isEmpty;

@Component
public class Utf8StringQueryParamResolver {

    public String doEncode(String input) {
        if (isEmpty(input)) {
            return null;
        }
        try {
            return encode(input, getEnc());
        } catch (UnsupportedEncodingException ue) {
            throw new QueryParamEncodeException(input, ue);
        }
    }

    public String doDecode(String input) {
        if (isEmpty(input)) {
            return null;
        }
        try {
            return decode(input, getEnc());
        } catch (UnsupportedEncodingException ue) {
            throw new QueryParamDecodeException(input, ue);
        }
    }

    public String getEnc() {
        return "UTF-8";
    }
}

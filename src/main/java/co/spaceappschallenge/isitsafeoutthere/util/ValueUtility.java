package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.exception.EmptyElementMapperNotFoundException;
import co.spaceappschallenge.isitsafeoutthere.util.computer.EmptyElementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Objects.isNull;

@Component
public class ValueUtility {

    private List<EmptyElementMapper> emptyElementMappers;

    @Autowired
    public ValueUtility(List<EmptyElementMapper> emptyElementMappers) {
        this.emptyElementMappers = emptyElementMappers;
    }

    private <T> EmptyElementMapper resolveComputer(Class<T> clazz) {
        for (EmptyElementMapper elementComputer : emptyElementMappers) {
            if (elementComputer.applies(clazz)) {
                return elementComputer;
            }
        }
        throw new EmptyElementMapperNotFoundException(clazz);
    }

    public <T> T emptyIfNull(T parameter, Class<T> clazz) {
        if (isNull(parameter)) {
            return resolveComputer(clazz).getEmptyElement();
        }
        return parameter;
    }

    public <T> T computeIfNull(T parameter, T resolveIfNull) {
        if (isNull(parameter)) {
            return resolveIfNull;
        }
        return parameter;
    }
}

package co.spaceappschallenge.isitsafeoutthere.util.computer;

public interface EmptyElementMapper {

    boolean applies(Class<?> clazz);

    <Value> Value getEmptyElement();
}

package co.spaceappschallenge.isitsafeoutthere.util.computer;

import org.springframework.stereotype.Component;

@Component
public class IntegerEmptyElementMapper implements EmptyElementMapper {

    @Override
    public boolean applies(Class<?> clazz) {
        return Integer.class.equals(clazz);
    }

    @Override
    public Integer getEmptyElement() {
        return 0;
    }
}

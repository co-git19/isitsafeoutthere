package co.spaceappschallenge.isitsafeoutthere.util.computer;

import org.springframework.stereotype.Component;

@Component
public class StringEmptyElementMapper implements EmptyElementMapper {

    @Override
    public boolean applies(Class<?> clazz) {
        return String.class.equals(clazz);
    }

    @Override
    public String getEmptyElement() {
        return "";
    }
}

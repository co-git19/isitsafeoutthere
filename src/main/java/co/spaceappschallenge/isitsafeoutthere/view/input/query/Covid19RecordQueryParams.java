package co.spaceappschallenge.isitsafeoutthere.view.input.query;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Objects;

public class Covid19RecordQueryParams {

    private String country;

    private String provinceState;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate endDate;

    private Covid19RecordQueryParams() {

    }

    public Covid19RecordQueryParams(String country, String provinceState, LocalDate startDate, LocalDate endDate) {

        this.country = country;
        this.provinceState = provinceState;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvinceState() {
        return provinceState;
    }

    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "Covid19RecordQueryParams{" +
                "country='" + country + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Covid19RecordQueryParams)) return false;
        Covid19RecordQueryParams that = (Covid19RecordQueryParams) o;
        return Objects.equals(country, that.country) &&
                Objects.equals(provinceState, that.provinceState) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, provinceState, startDate, endDate);
    }
}

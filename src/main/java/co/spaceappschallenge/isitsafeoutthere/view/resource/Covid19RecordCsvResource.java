package co.spaceappschallenge.isitsafeoutthere.view.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Covid19RecordCsvResource {

    @JsonProperty("Province/State")
    public String legacyProvinceState;

    @JsonProperty("Country/Region")
    public String legacyCountryRegion;

    @JsonProperty("Province_State")
    public String currentProvinceState;

    @JsonProperty("Country_Region")
    public String currentCountryRegion;

    @JsonProperty("Admin2")
    public String region;

    @JsonProperty("Confirmed")
    public Integer confirmed;

    @JsonProperty("Deaths")
    public Integer deaths;

    @JsonProperty("Recovered")
    public Integer recovered;

}

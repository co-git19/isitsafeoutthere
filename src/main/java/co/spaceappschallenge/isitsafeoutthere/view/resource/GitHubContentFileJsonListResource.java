package co.spaceappschallenge.isitsafeoutthere.view.resource;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GitHubContentFileJsonListResource extends ArrayList<GitHubContentFileJsonResource> {
}

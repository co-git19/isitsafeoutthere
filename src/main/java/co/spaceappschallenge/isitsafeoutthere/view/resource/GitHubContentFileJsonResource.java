package co.spaceappschallenge.isitsafeoutthere.view.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GitHubContentFileJsonResource {

    @JsonProperty("name")
    public String name;

    @JsonProperty("download_url")
    public String downloadUrl;

}

package co.spaceappschallenge.isitsafeoutthere.view.response;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Relation("covid19records")
public class Covid19RecordResponseResource {

    private Long id;
    private String country;
    private String provinceState;
    private String region;
    private LocalDate referenceDate;
    private Integer confirmed;
    private Integer recovered;
    private Integer deaths;
    private String source;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;

    private Covid19RecordResponseResource() {

    }

    public Covid19RecordResponseResource(Covid19Record entity) {
        this(entity.getId(), entity.getCountry(), entity.getProvinceState(), entity.getRegion(),
                entity.getReferenceDate(), entity.getConfirmed(), entity.getRecovered(),  entity.getDeaths(),
                entity.getSource(), entity.getCreatedOn(), entity.getUpdatedOn());
    }

    public Covid19RecordResponseResource(Long id, String country, String provinceState, String region,
                                         LocalDate referenceDate, Integer confirmed, Integer recovered,
                                         Integer deaths,  String source, LocalDateTime createdOn,
                                         LocalDateTime updatedOn) {
        this.id = id;
        this.country = country;
        this.provinceState = provinceState;
        this.region = region;
        this.referenceDate = referenceDate;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
        this.source = source;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvinceState() {
        return provinceState;
    }

    public void setProvinceState(String provinceState) {
        this.provinceState = provinceState;
    }

    public LocalDate getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(LocalDate referenceDate) {
        this.referenceDate = referenceDate;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "Covid19RecordResponseResource{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", provinceState='" + provinceState + '\'' +
                ", referenceDate=" + referenceDate +
                ", confirmed=" + confirmed +
                ", recovered=" + recovered +
                ", deaths=" + deaths +
                ", source='" + source + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

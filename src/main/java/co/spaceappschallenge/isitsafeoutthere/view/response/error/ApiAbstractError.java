package co.spaceappschallenge.isitsafeoutthere.view.response.error;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class ApiAbstractError {

    private String message;

    protected ApiAbstractError() { }

    public ApiAbstractError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

package co.spaceappschallenge.isitsafeoutthere.view.response.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.Clock.systemUTC;
import static java.time.LocalDateTime.now;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiError extends ApiAbstractError {

    private HttpStatus status;

    private LocalDateTime timestamp;

    private String debugMessage;

    private List<ApiAbstractError> details;

    private ApiError() {
        this(null, null, null, null);
    }

    public ApiError(HttpStatus status) {
        this(status.toString(), status, null, null);
    }

    public ApiError(String message, HttpStatus status) {
        this(message, status, null, null);
    }

    public ApiError(String message, HttpStatus status, String debugMessage) {
        this(message, status, debugMessage, null);
    }

    public ApiError(String message, HttpStatus status, String debugMessage,
                    List<ApiAbstractError> details) {
        super(message);
        this.timestamp = now(systemUTC());
        this.status = status;
        this.debugMessage = debugMessage;
        this.details = details;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    public List<ApiAbstractError> getDetails() {
        return details;
    }

    public void setDetails(List<ApiAbstractError> details) {
        this.details = details;
    }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {HomeScreenComponent} from "./home-screen/home-screen.component";
import {PlotsScreenComponent} from "./plots-screen/plots-screen.component";
import {InsightsScreenComponent} from "./insights-screen/insights-screen.component";

@NgModule({
  declarations: [
    AppComponent,
    InsightsScreenComponent,
    PlotsScreenComponent,
    HomeScreenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsightsScreenComponent } from './insights-screen.component';

describe('InsightsScreenComponent', () => {
  let component: InsightsScreenComponent;
  let fixture: ComponentFixture<InsightsScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsightsScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsightsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

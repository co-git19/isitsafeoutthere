import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotsScreenComponent } from './plots-screen.component';

describe('PlotsScreenComponent', () => {
  let component: PlotsScreenComponent;
  let fixture: ComponentFixture<PlotsScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlotsScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlotsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

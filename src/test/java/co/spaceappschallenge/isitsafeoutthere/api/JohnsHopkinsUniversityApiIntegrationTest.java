package co.spaceappschallenge.isitsafeoutthere.api;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootProfiledTest
public class JohnsHopkinsUniversityApiIntegrationTest {

    @Autowired
    private JohnsHopkinsUniversityApi johnsHopkinsUniversityApi;

    @Test
    public void shouldReturnAllDataInsideFolder() {
        GitHubContentFileJsonListResource result = johnsHopkinsUniversityApi.getContentFileResources();

        assertNotNull(result);
    }

    @Test
    public void shouldReturnDataForGivenURI() {
        List<Covid19RecordCsvResource> result = johnsHopkinsUniversityApi.getCollectedDataFromUri("https://raw" +
                ".githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/ " +
                "csse_covid_19_daily_reports/05-29-2020.csv");

        assertNotNull(result);
    }

}
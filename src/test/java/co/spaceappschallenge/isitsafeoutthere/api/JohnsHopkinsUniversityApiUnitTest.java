package co.spaceappschallenge.isitsafeoutthere.api;

import co.spaceappschallenge.isitsafeoutthere.api.uri.JohnsHopkinsUniversityUriGenerator;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsCsvMapper;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JohnsHopkinsUniversityApiUnitTest {

    @Mock
    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private Covid19RecordsCsvMapper csvMapper;

    @InjectMocks
    private JohnsHopkinsUniversityApi johnsHopkinsUniversityApi;

    @Test
    public void shouldCallRestTemplateGetEndpointWhenRequestingForFiles() {
        final URI calledURI = URI.create("dummy-uri");
        when(uriGenerator.getContentsPath()).thenReturn(calledURI);

        johnsHopkinsUniversityApi.getContentFileResources();

        ArgumentCaptor<URI> uriArgumentCaptor = ArgumentCaptor.forClass(URI.class);
        verify(restTemplate, times(1)).getForObject(uriArgumentCaptor.capture(),
                eq(GitHubContentFileJsonListResource.class));
        assertEquals(calledURI, uriArgumentCaptor.getValue());
    }

    @Test
    public void shouldCallRestTemplateAndMapperGetEndpointWhenRequestingForFiles() {
        final URI calledURI = URI.create("dummy-uri");
        when(uriGenerator.getContentsPath()).thenReturn(calledURI);

        johnsHopkinsUniversityApi.getContentFileResources();

        ArgumentCaptor<URI> uriArgumentCaptor = ArgumentCaptor.forClass(URI.class);
        verify(restTemplate, times(1)).getForObject(uriArgumentCaptor.capture(),
                eq(GitHubContentFileJsonListResource.class));
        assertEquals(calledURI, uriArgumentCaptor.getValue());
    }

    @Test
    public void shouldCallRestTemplateAndMapperWhenRequestingForCollectedData() {
        when(restTemplate.getForObject(anyString(), any())).thenReturn("data-contents");

        johnsHopkinsUniversityApi.getCollectedDataFromUri("my-raw-download-file-uri");

        verify(restTemplate, times(1)).getForObject(eq("my-raw-download-file-uri"), eq(String.class));
        verify(csvMapper, times(1)).readResourceFromCommaSeparatedString(eq("data-contents"));
    }

}
package co.spaceappschallenge.isitsafeoutthere.api.uri;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.util.StringUtils.isEmpty;

@SpringBootProfiledTest
public class JohnsHopkinsUniversityUriGeneratorIntegrationTest {

    @Autowired
    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    @Test
    public void shouldInjectProperties() {

        assertFalse(isEmpty(uriGenerator.getScheme()));
        assertFalse(isEmpty(uriGenerator.getHost()));
        assertNotEquals(0, uriGenerator.getPort());
        assertFalse(isEmpty(uriGenerator.getContextPath()));
    }
}
package co.spaceappschallenge.isitsafeoutthere.api.uri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class JohnsHopkinsUniversityUriGeneratorUnitTest {

    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    @Before
    public void setUp() {
        uriGenerator = new JohnsHopkinsUniversityUriGenerator("https", "api.github.com", 443, "/", "CSSEGISandData",
                "COVID-19", "csse_covid_19_data/csse_covid_19_daily_reports");
    }

    @Test
    public void shouldReturnBasePath() {
        String result = uriGenerator.getBasePath().build().toUri().toString();

        assertEquals("https://api.github.com:443/", result);
    }

    @Test
    public void shouldReturnContentsPath() {
        String result = uriGenerator.getContentsPath().toString();

        assertEquals("https://api.github.com:443/repos/CSSEGISandData/COVID-19/contents/csse_covid_19_data" +
                "/csse_covid_19_daily_reports", result);
    }

    @Test
    public void shouldReturnContentsFilePath() {
        String result = uriGenerator.getContentsFilePath("01-24-2020.csv").toString();

        assertEquals("https://api.github.com:443/repos/CSSEGISandData/COVID-19/contents/csse_covid_19_data" +
                "/csse_covid_19_daily_reports/01-24-2020.csv", result);
    }


}
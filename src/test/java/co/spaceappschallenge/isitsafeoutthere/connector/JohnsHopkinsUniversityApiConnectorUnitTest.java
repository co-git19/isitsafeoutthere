package co.spaceappschallenge.isitsafeoutthere.connector;

import co.spaceappschallenge.isitsafeoutthere.api.JohnsHopkinsUniversityApi;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsResourceMapper;
import co.spaceappschallenge.isitsafeoutthere.mapper.GitHubFileResourceMapper;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JohnsHopkinsUniversityApiConnectorUnitTest {

    @Mock
    private GitHubFileResourceMapper fileResourceMapper;

    @Mock
    private Covid19RecordsResourceMapper recordsResourceMapper;

    @Mock
    private JohnsHopkinsUniversityApi api;

    @InjectMocks
    private JohnsHopkinsUniversityApiConnector connector;

    @Before
    public void setUp() {
        when(api.getContentFileResources()).thenReturn(new GitHubContentFileJsonListResource());
    }

    @Test
    public void shouldCallMapperAndApiWhenRequestingForGitHubFiles() {
        when(fileResourceMapper.fromResourceList(any(GitHubContentFileJsonListResource.class))).thenReturn(emptyList());

        connector.getJohnsHopkinsUniversityRepositoryData();

        verify(api, times(1)).getContentFileResources();
        verify(fileResourceMapper, times(1)).fromResourceList(any(GitHubContentFileJsonListResource.class));
    }

    @Test
    public void shouldRemoveRepetitionsWhenRequestingForGitHubFiles() {
        when(fileResourceMapper.fromResourceList(any(GitHubContentFileJsonListResource.class))).thenReturn(asList(
                new GitHubContentFile("name", "url"),
                new GitHubContentFile("name", "url")));

        Set<GitHubContentFile> result = connector.getJohnsHopkinsUniversityRepositoryData();

        assertEquals(1, result.size());
    }

    @Test
    public void shouldCallMapperAndApiWhenRequestingForCovid19Records() {
        when(recordsResourceMapper.fromResourceList(anyString(), anyList())).thenReturn(emptyList());

        connector.getCovidCasesIncidenceAcccordingToJohnsHopkins("fileName", "downloadUri");

        verify(api, times(1)).getCollectedDataFromUri(eq("downloadUri"));
        verify(recordsResourceMapper, times(1)).fromResourceList(eq("fileName"), eq(emptyList()));
    }

    @Test
    public void shouldRemoveRepetitionsWhenRequestingForCovid19Records() {
        when(recordsResourceMapper.fromResourceList(anyString(), anyList())).thenReturn(asList(
                new Covid19RecordResourceTestBuilder().referenceDate(LocalDate.of(2020, 5, 1)).build(),
                new Covid19RecordResourceTestBuilder().referenceDate(LocalDate.of(2020, 5, 1)).build()
        ));

        Set<Covid19RecordResource> result = connector.getCovidCasesIncidenceAcccordingToJohnsHopkins("bla", "bla");

        assertEquals(1, result.size());
    }

    @Test
    public void shouldKeepDifferentRecordsWhenRequestingForCovid19Records() {
        when(recordsResourceMapper.fromResourceList(anyString(), anyList())).thenReturn(asList(
                new Covid19RecordResourceTestBuilder().build(),
                new Covid19RecordResourceTestBuilder().country("US").provinceState("Alabama").build()));

        Set<Covid19RecordResource> result = connector.getCovidCasesIncidenceAcccordingToJohnsHopkins("bla", "bla");

        assertEquals(2, result.size());
    }

}
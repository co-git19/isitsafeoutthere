package co.spaceappschallenge.isitsafeoutthere.controller;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.service.Covid19RecordService;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParamsTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import co.spaceappschallenge.isitsafeoutthere.view.response.error.ApiError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootProfiledTest
@AutoConfigureMockMvc
public class Covid19RecordControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Covid19RecordController controller;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private Covid19RecordService service;

    @Test
    public void shouldReturnFindOneWithSelfLinks() throws Exception {
        final Covid19Record entity = new Covid19RecordTestBuilder().build();
        when(service.findOne(1L)).thenReturn(Optional.of(entity));
        final Covid19RecordResponseResource result = new Covid19RecordResponseResource(entity);

        mockMvc.perform(get("/records/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(result)))
                .andExpect(jsonPath("$._links.self.href", equalTo("http://localhost/records/{id}")));
    }

    @Test
    public void shouldReturn404WithExpectedMessage() throws Exception {
        final ApiError apiError = new ApiError("No COVID-19 record found for id 1.", NOT_FOUND);

        mockMvc.perform(get("/records/1"))
            .andDo(print())
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message", equalTo(apiError.getMessage())))
            .andExpect(jsonPath("$.status", equalTo("NOT_FOUND")))
            .andExpect(jsonPath("$.timestamp", notNullValue()));
    }

    @Test
    public void shouldCallFindAllEndpoint() throws Exception {
        when(service.findAll(any())).thenReturn(Page.empty());

        mockMvc.perform(get("/records"))
                .andDo(print())
                .andExpect(status().isOk());

        Pageable expected = PageRequest.of(0, 10);
        verify(service, times(1)).findAll(eq(expected));
    }

    @Test
    public void shouldCallFindByStartDateEndpoint() throws Exception {
        when(service.findByQueryParams(any(), any())).thenReturn(Page.empty());

        mockMvc.perform(get("/records?startDate=2020-01-01"))
                .andDo(print())
                .andExpect(status().isOk());

        Pageable expected = PageRequest.of(0, 10);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .endDate(null)
                .build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

    @Test
    public void shouldCallFindByEndDateEndpoint() throws Exception {
        when(service.findByQueryParams(any(), any())).thenReturn(Page.empty());

        mockMvc.perform(get("/records?endDate=2020-12-31"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.self.href",
                        equalTo("http://localhost/records?endDate=2020-12-31&page=0&size=10")));

        Pageable expected = PageRequest.of(0, 10);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .startDate(null)
                .build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

    @Test
    public void shouldCallFindByEndDateAndCountryEndpoint() throws Exception {
        when(service.findByQueryParams(any(), any())).thenReturn(Page.empty());

        mockMvc.perform(get("/records?endDate=2020-12-31&country=Brazil"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.self.href",
                        equalTo("http://localhost/records?country=Brazil&endDate=2020-12-31&page=0&size=10")));

        Pageable expected = PageRequest.of(0, 10);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .provinceState(null)
                .startDate(null)
                .build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

    @Test
    public void shouldCallFindByStartDateAndEndDateAndFilterPageResults() throws Exception {
        when(service.findByQueryParams(any(), any())).thenReturn(new PageImpl<>(asList(
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build()
        ), PageRequest.of(0, 2), 20));

        mockMvc.perform(get("/records?startDate=2020-01-01&endDate=2020-12-31&country=Brazil&page=0&size=2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.self.href",
                        equalTo("http://localhost/records?country=Brazil&startDate=2020-01-01&endDate=2020-12-31&page" +
                                "=0&size=2")));

        Pageable expected = PageRequest.of(0, 2);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .provinceState(null)
                .build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

    @Test
    public void shouldReturnEncodedSelfLink() throws Exception {

        when(service.findByQueryParams(any(), any())).thenReturn(new PageImpl<>(asList(
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build()
        ), PageRequest.of(0, 2), 20));

        mockMvc.perform(get("/records?provinceState=Minas Gerais&startDate=2020-01-01&endDate=2020-12-31"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.self.href",
                        equalTo("http://localhost/records?provinceState=Minas+Gerais&startDate=2020-01-01" +
                                "&endDate=2020-12-31&page=0&size=10")));

        Pageable expected = PageRequest.of(0, 10);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder().country(null).build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

    @Test
    public void shouldDecodeUtf8ParamsCorrectly() throws Exception {

        when(service.findByQueryParams(any(), any())).thenReturn(new PageImpl<>(asList(
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build()
        ), PageRequest.of(0, 2), 2));

        mockMvc.perform(get("/records?provinceState=Minas+Gerais&startDate=2020-01-01&endDate=2020-12-31"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links.self.href",
                        equalTo("http://localhost/records?provinceState=Minas+Gerais&startDate=2020-01-01" +
                                "&endDate=2020-12-31&page=0&size=10")));

        Pageable expected = PageRequest.of(0, 10);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder().country(null).build();
        verify(service, times(1)).findByQueryParams(eq(expected), eq(queryParams));
    }

}
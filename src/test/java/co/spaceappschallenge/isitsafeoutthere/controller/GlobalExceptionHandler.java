package co.spaceappschallenge.isitsafeoutthere.controller;

import co.spaceappschallenge.isitsafeoutthere.view.response.error.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<ApiError> handleNotFound(NoSuchElementException exception) {
        LOGGER.error("Tried to request for element that does not exist. Details:", exception);
        return buildResponseEntity(new ApiError(exception.getMessage(), NOT_FOUND));
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    protected ResponseEntity<ApiError> handleUnsupportedOperation(UnsupportedOperationException exception) {
        LOGGER.error("Tried to execute an operation that's not supported. Details:", exception);
        return buildResponseEntity(new ApiError(exception.getMessage(), NOT_IMPLEMENTED));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<ApiError> handleIllegalArgumentException(IllegalArgumentException exception) {
        LOGGER.error("Used wrong parameters for API query:", exception);
        return buildResponseEntity(new ApiError(exception.getMessage(), BAD_REQUEST));
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<ApiError> handleRuntimeException(RuntimeException exception) {
        LOGGER.error("Tried to decode using wrong encryption:", exception);
        return buildResponseEntity(new ApiError(exception.getMessage(), INTERNAL_SERVER_ERROR));
    }

    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}

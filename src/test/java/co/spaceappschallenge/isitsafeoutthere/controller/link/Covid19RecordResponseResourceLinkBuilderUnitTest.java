package co.spaceappschallenge.isitsafeoutthere.controller.link;

import co.spaceappschallenge.isitsafeoutthere.controller.link.assembler.Covid19RecordResponseResourceAssembler;
import co.spaceappschallenge.isitsafeoutthere.util.Utf8StringQueryParamResolver;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParamsTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResourceTestBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class Covid19RecordResponseResourceLinkBuilderUnitTest {

    @Mock
    private Covid19RecordResponseResourceAssembler assembler;

    @Mock
    private HateoasPageableHandlerMethodArgumentResolver argumentResolver;

    @Mock
    private Utf8StringQueryParamResolver paramResolver;

    @InjectMocks
    private Covid19RecordResponseResourceLinkBuilder linkBuilder;

    @Mock
    private PagedResourcesAssembler<Covid19RecordResponseResource> pageAssembler;

    @Before
    public void setUp() {
        when(paramResolver.doEncode(anyString())).thenReturn("encoded+param");
    }

    @Test
    public void shouldWrapEntityIntoEntityModel() {
        final Covid19RecordResponseResource resource = new Covid19RecordResponseResourceTestBuilder().build();

        linkBuilder.addFindOneLink(resource);

        verify(assembler, times(1)).toModel(same(resource));
    }

    @Test
    public void shouldWrapPageIntoPagedModel() {
        final Covid19RecordResponseResource resource = new Covid19RecordResponseResourceTestBuilder().build();
        final Page<Covid19RecordResponseResource> page = new PageImpl<>(singletonList(resource));

        linkBuilder.addFindAllLinks(page, pageAssembler);

        verify(pageAssembler, times(1)).toModel(same(page), eq(assembler));
    }

    @Test
    public void shouldAddQueryParamsLinks() {
        Page<Covid19RecordResponseResource> entities = new PageImpl<>(
                singletonList(new Covid19RecordResponseResourceTestBuilder().build()));
        final Pageable pageable = PageRequest.of(0, 20);

        linkBuilder.addQueryParamsLinks(entities, pageAssembler, pageable,
                new Covid19RecordQueryParamsTestBuilder().build());

        Link result = Link.of("/records?country=encoded+param&provinceState=encoded+param&startDate=2020-01-01" +
                "&endDate=2020-12-31", IanaLinkRelations.SELF);
        ArgumentCaptor<Link> linkArgumentCaptor = ArgumentCaptor.forClass(Link.class);
        verify(pageAssembler, times(1)).toModel(eq(entities), eq(assembler), linkArgumentCaptor.capture());
        assertEquals(result.getHref(), linkArgumentCaptor.getValue().getHref());
        ArgumentCaptor<UriComponentsBuilder> componentsBuilderArgumentCaptor =
                ArgumentCaptor.forClass(UriComponentsBuilder.class);
        verify(argumentResolver, times(1)).enhance(componentsBuilderArgumentCaptor.capture(), isNull(), eq(pageable));
        UriComponents uri = componentsBuilderArgumentCaptor.getValue().build();
        assertEquals(4, uri.getQueryParams().size());
        assertTrue(uri.getQueryParams().keySet().containsAll(newSet("country", "provinceState", "startDate",
                "endDate")));
        verify(paramResolver, times(1)).doEncode(eq("Brazil"));
        verify(paramResolver, times(1)).doEncode(eq("Minas Gerais"));
    }

    @Test
    public void shouldAddQueryParamsWithoutCountryLinks() {
        Page<Covid19RecordResponseResource> entities = new PageImpl<>(
                singletonList(new Covid19RecordResponseResourceTestBuilder().build()));

        linkBuilder.addQueryParamsLinks(entities, pageAssembler, PageRequest.of(0, 20),
                new Covid19RecordQueryParamsTestBuilder().country(null).build());

        Link result = Link.of("/records?provinceState=encoded+param&startDate=2020-01-01&endDate=2020-12-31",
                IanaLinkRelations.SELF);
        ArgumentCaptor<Link> linkArgumentCaptor = ArgumentCaptor.forClass(Link.class);
        verify(pageAssembler, times(1)).toModel(eq(entities), eq(assembler), linkArgumentCaptor.capture());
        assertEquals(result.getHref(), linkArgumentCaptor.getValue().getHref());
        verify(paramResolver, times(1)).doEncode(eq("Minas Gerais"));
        verify(paramResolver, times(1)).doEncode(isNull());
    }

    @Test
    public void shouldAddQueryParamsWithoutProvinceStateAndCountryLinks() {
        Page<Covid19RecordResponseResource> entities = new PageImpl<>(
                singletonList(new Covid19RecordResponseResourceTestBuilder().build()));

        linkBuilder.addQueryParamsLinks(entities, pageAssembler, PageRequest.of(0, 20),
                new Covid19RecordQueryParamsTestBuilder().country(null).provinceState(null).build());

        Link result = Link.of("/records?startDate=2020-01-01&endDate=2020-12-31",
                IanaLinkRelations.SELF);
        ArgumentCaptor<Link> linkArgumentCaptor = ArgumentCaptor.forClass(Link.class);
        verify(pageAssembler, times(1)).toModel(eq(entities), eq(assembler), linkArgumentCaptor.capture());
        assertEquals(result.getHref(), linkArgumentCaptor.getValue().getHref());
    }

    @Test
    public void shouldAddQueryParamsWithoutProvinceStateAndCountryAndStartDateLinks() {
        Page<Covid19RecordResponseResource> entities = new PageImpl<>(
                singletonList(new Covid19RecordResponseResourceTestBuilder().build()));

        linkBuilder.addQueryParamsLinks(entities, pageAssembler, PageRequest.of(0, 20),
                new Covid19RecordQueryParamsTestBuilder().country(null).provinceState(null).startDate(null).build());

        Link result = Link.of("/records?endDate=2020-12-31",
                IanaLinkRelations.SELF);
        ArgumentCaptor<Link> linkArgumentCaptor = ArgumentCaptor.forClass(Link.class);
        verify(pageAssembler, times(1)).toModel(eq(entities), eq(assembler), linkArgumentCaptor.capture());
        assertEquals(result.getHref(), linkArgumentCaptor.getValue().getHref());
    }

}
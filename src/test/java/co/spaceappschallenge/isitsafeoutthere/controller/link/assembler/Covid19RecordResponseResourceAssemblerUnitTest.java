package co.spaceappschallenge.isitsafeoutthere.controller.link.assembler;

import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResourceTestBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class Covid19RecordResponseResourceAssemblerUnitTest {

    @Mock
    private PagedResourcesAssembler<Covid19RecordResponseResource> pageAssembler;

    @Mock
    private HateoasPageableHandlerMethodArgumentResolver argumentResolver;

    @InjectMocks
    private Covid19RecordResponseResourceAssembler assembler;

    @Test
    public void shouldAddSelfLinkWhenUsingAssembler() {
        EntityModel<Covid19RecordResponseResource> entity =
                EntityModel.of(new Covid19RecordResponseResourceTestBuilder().build());

        assembler.addLinks(entity);

        Link result = Link.of("/records/1", IanaLinkRelations.SELF);
        assertTrue(entity.getLink(IanaLinkRelations.SELF).isPresent());
        assertEquals(result.getHref(), entity.getLink(IanaLinkRelations.SELF).get().getHref());
    }

    @Test
    public void shouldAddSelfLinkWhenUsingAssemblerOnList() {
        CollectionModel<EntityModel<Covid19RecordResponseResource>> entities = CollectionModel
                .of(singletonList(EntityModel.of(new Covid19RecordResponseResourceTestBuilder().build())));

        assembler.addLinks(entities);

        Link result = Link.of("/records/1", IanaLinkRelations.SELF);
        entities.getContent().forEach(entity -> {
            assertTrue(entity.getLink(IanaLinkRelations.SELF).isPresent());
            assertEquals(result.getHref(), entity.getLink(IanaLinkRelations.SELF).get().getHref());
        });
    }

}
package co.spaceappschallenge.isitsafeoutthere.domain.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Covid19RecordTestBuilder {

    private final Covid19Record record;

    public Covid19RecordTestBuilder() {
        record = new Covid19Record("Brazil", "Minas Gerais", "BH", LocalDate.now(), 0, 0,
                0, "source");
    }

    public Covid19RecordTestBuilder(String country, String provinceState, String region, LocalDate referenceDate,
                                    Integer confirmed, Integer recovered, Integer deaths, String source) {
        record = new Covid19Record(country, provinceState, region, referenceDate, confirmed, recovered, deaths,
                source);
    }

    public Covid19RecordTestBuilder(Covid19Record baseResource) {
        record = new Covid19Record(baseResource);
    }

    public Covid19RecordTestBuilder(Long id, LocalDateTime createdOn, LocalDateTime updatedOn) {
        this();
        id(id);
        createdOn(createdOn);
        updatedOn(updatedOn);
    }

    public Covid19RecordTestBuilder id(Long id) {
        record.setId(id);
        return this;
    }

    public Covid19RecordTestBuilder country(String country) {
        record.setCountry(country);
        return this;
    }

    public Covid19RecordTestBuilder provinceState(String provinceState) {
        record.setProvinceState(provinceState);
        return this;
    }

    public Covid19RecordTestBuilder region(String region) {
        record.setRegion(region);
        return this;
    }

    public Covid19RecordTestBuilder referenceDate(LocalDate referenceDate) {
        record.setReferenceDate(referenceDate);
        return this;
    }

    public Covid19RecordTestBuilder confirmed(Integer confirmed) {
        record.setConfirmed(confirmed);
        return this;
    }

    public Covid19RecordTestBuilder recovered(Integer recovered) {
        record.setRecovered(recovered);
        return this;
    }

    public Covid19RecordTestBuilder deaths(Integer deaths) {
        record.setDeaths(deaths);
        return this;
    }

    public Covid19RecordTestBuilder source(String source) {
        record.setSource(source);
        return this;
    }

    public Covid19RecordTestBuilder createdOn(LocalDateTime createdOn) {
        record.setCreatedOn(createdOn);
        return this;
    }

    public Covid19RecordTestBuilder updatedOn(LocalDateTime updatedOn) {
        record.setUpdatedOn(updatedOn);
        return this;
    }

    public Covid19Record build() {
        return record;
    }
}
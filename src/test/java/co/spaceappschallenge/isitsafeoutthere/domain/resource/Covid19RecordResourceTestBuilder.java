package co.spaceappschallenge.isitsafeoutthere.domain.resource;

import java.time.LocalDate;

import static java.time.LocalDate.now;

public class Covid19RecordResourceTestBuilder {

    private final Covid19RecordResource recordResource;

    public Covid19RecordResourceTestBuilder() {
        recordResource = new Covid19RecordResource("Brazil", "Minas Gerais", "BH", now(), 0, 0,
                0, "source");
    }

    public Covid19RecordResourceTestBuilder(String country, String provinceState, String region,
                                            LocalDate referenceDate, Integer confirmed, Integer recovered,
                                            Integer deaths, String source) {
        recordResource = new Covid19RecordResource(country, provinceState, region, referenceDate, confirmed, recovered,
                deaths, source);
    }

    public Covid19RecordResourceTestBuilder(Covid19RecordResource baseResource) {
        recordResource = new Covid19RecordResource(baseResource.getCountry(), baseResource.getProvinceState(),
                baseResource.getRegion(), baseResource.getReferenceDate(), baseResource.getConfirmed(),
                baseResource.getRecovered(), baseResource.getDeaths(), baseResource.getSource());
    }

    public Covid19RecordResourceTestBuilder country(String country) {
        recordResource.setCountry(country);
        return this;
    }

    public Covid19RecordResourceTestBuilder provinceState(String provinceState) {
        recordResource.setProvinceState(provinceState);
        return this;
    }

    public Covid19RecordResourceTestBuilder referenceDate(LocalDate referenceDate) {
        recordResource.setReferenceDate(referenceDate);
        return this;
    }

    public Covid19RecordResourceTestBuilder confirmed(Integer confirmed) {
        recordResource.setConfirmed(confirmed);
        return this;
    }

    public Covid19RecordResourceTestBuilder recovered(Integer recovered) {
        recordResource.setRecovered(recovered);
        return this;
    }

    public Covid19RecordResourceTestBuilder deaths(Integer deaths) {
        recordResource.setDeaths(deaths);
        return this;
    }

    public Covid19RecordResourceTestBuilder source(String source) {
        recordResource.setSource(source);
        return this;
    }

    public Covid19RecordResource build() {
        return recordResource;
    }

}
package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.view.response.Covid19RecordResponseResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class Covid19RecordControllerMapperUnitTest {

    private Covid19RecordControllerMapper mapper;

    @Before
    public void setUp() {
        mapper = new Covid19RecordControllerMapper();
    }

    @Test
    public void shouldMapPagedEntities() {
        Covid19Record input = new Covid19RecordTestBuilder().build();

        Page<Covid19RecordResponseResource> result = mapper.fromPage(new PageImpl<>(singletonList(input),
                PageRequest.of(0, 1), 1000));

        assertEquals(1, result.getNumberOfElements());
        Covid19RecordResponseResource output = result.getContent().get(0);
        assertEquals(input.getId(), output.getId());
        assertEquals(input.getCountry(), output.getCountry());
        assertEquals(input.getProvinceState(), output.getProvinceState());
        assertEquals(input.getReferenceDate(), output.getReferenceDate());
        assertEquals(input.getConfirmed(), output.getConfirmed());
        assertEquals(input.getRecovered(), output.getRecovered());
        assertEquals(input.getDeaths(), output.getDeaths());
        assertEquals(input.getCreatedOn(), output.getCreatedOn());
        assertEquals(input.getUpdatedOn(), output.getUpdatedOn());
    }

}
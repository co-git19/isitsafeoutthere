package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

import static java.time.LocalDate.now;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class Covid19RecordUpdaterUnitTest {

    private Covid19RecordUpdater updater;

    @Before
    public void setUp() {
        updater = new Covid19RecordUpdater();
    }

    @Test
    public void shouldCopyUpdatesToNewMemoryAddress() {
        LocalDate referenceDate = now();
        Covid19Record currentState = new Covid19RecordTestBuilder().referenceDate(referenceDate).build();
        Covid19Record updatedState = new Covid19RecordTestBuilder()
                .referenceDate(referenceDate)
                .confirmed(4)
                .recovered(5)
                .deaths(6)
                .source("newSource")
                .build();

        Covid19Record result = updater.doUpdate(currentState, updatedState);

        assertNotSame(currentState, result);
        assertEquals("Brazil", result.getCountry());
        assertEquals("Minas Gerais", result.getProvinceState());
        assertEquals(currentState.getReferenceDate(), result.getReferenceDate());
        assertEquals(4, result.getConfirmed());
        assertEquals(5, result.getRecovered());
        assertEquals(6, result.getDeaths());
        assertEquals("newSource", result.getSource());
    }

}
package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.internal.util.collections.Sets.newSet;

@SpringBootProfiledTest
public class Covid19RecordsCsvMapperIntegrationTest {

    @Autowired
    private Covid19RecordsCsvMapper mapper;

    @Test
    public void shouldConverCurrentResources() {

        List<Covid19RecordCsvResource> currentResults = mapper
                .readResourceFromCommaSeparatedString(
                        "Province_State,Country_Region,Confirmed,Deaths,Recovered\n" +
                                "Minas Gerais,Brazil,28,,1\n" +
                                "Acre,Brazil,5,,0\n" +
                                "São Paulo,Brazil,8000,,56");

        Set<String> provinceStateValues = newSet("Minas Gerais", "Acre", "São Paulo");
        Set<String> countryRegionValues = newSet("Brazil");
        Set<Integer> confirmedValues = newSet(28, 5, 8000);
        Set<Integer> recoveredValues = newSet(0, 1, 56);

        assertEquals(3, currentResults.size());
        currentResults
                .forEach(record -> {
                    assertTrue(provinceStateValues.contains(record.currentProvinceState));
                    assertTrue(countryRegionValues.contains(record.currentCountryRegion));
                    assertTrue(confirmedValues.contains(record.confirmed));
                    assertTrue(recoveredValues.contains(record.recovered));
                    assertNull(record.deaths);
                });
    }

    @Test
    public void shouldConvertLegacyResource() {

        List<Covid19RecordCsvResource> legacyResults = mapper
                .readResourceFromCommaSeparatedString(
                        "Province/State,Country/Region,Confirmed,Deaths,Recovered\n" +
                                "Minas Gerais,Brazil,28,,1\n" +
                                "Acre,Brazil,5,,0\n" +
                                "São Paulo,Brazil,8000,,56");
        assertEquals(1, 1);

        Set<String> provinceStateValues = newSet("Minas Gerais", "Acre", "São Paulo");
        Set<String> countryRegionValues = newSet("Brazil");
        Set<Integer> confirmedValues = newSet(28, 5, 8000);
        Set<Integer> recoveredValues = newSet(0, 1, 56);

        assertEquals(3, legacyResults.size());

        legacyResults
                .forEach(record -> {
                    assertTrue(provinceStateValues.contains(record.legacyProvinceState));
                    assertTrue(countryRegionValues.contains(record.legacyCountryRegion));
                    assertTrue(confirmedValues.contains(record.confirmed));
                    assertTrue(recoveredValues.contains(record.recovered));
                    assertNull(record.deaths);
                });
    }

    @Test
    public void shouldThrowRuntimeExceptionGivenProblemInCsv() {
        RuntimeException re = null;

        try {
            List<Covid19RecordCsvResource> legacyResults = mapper
                    .readResourceFromCommaSeparatedString(
                            "Province/State,Country/Region,Confirmed,Deaths,Recovered\n" +
                                    "Minas Gerais,Brazil,28,,1,,,,,,,\n" +
                                    "Acre,Brazil,5,,,0\n" +
                                    "São Paulo,,Brazil,8000,,56");
        } catch (RuntimeJsonMappingException rje) {
            re = rje;
        }

        assertNotNull(re);
    }
}
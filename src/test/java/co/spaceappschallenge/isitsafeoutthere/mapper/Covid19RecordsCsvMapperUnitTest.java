package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.exception.Covid19RecordConversionException;
import co.spaceappschallenge.isitsafeoutthere.util.CsvBasicHeaderConverter;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Covid19RecordsCsvMapperUnitTest {

    @Mock
    private CsvBasicHeaderConverter basicHeaderConverter;

    @Mock
    private ObjectReader objectReader;

    @Mock
    private MappingIterator mappingIterator;

    @InjectMocks
    private Covid19RecordsCsvMapper mapper;

    @Before
    public void setUp() {
        when(basicHeaderConverter.getForClass(any())).thenReturn(objectReader);
    }

    @Test
    public void shouldMapCsvToResources() throws IOException {
        when(objectReader.readValues(anyString())).thenReturn(mappingIterator);
        when(mappingIterator.hasNext()).thenReturn(true).thenReturn(false);
        when(mappingIterator.next()).thenReturn(new Covid19RecordCsvResource());

        List<Covid19RecordCsvResource> results = mapper.readResourceFromCommaSeparatedString("any,csv,data");

        assertEquals(1, results.size());
        verify(basicHeaderConverter, times(1)).getForClass(eq(Covid19RecordCsvResource.class));
        verify(objectReader, times(1)).readValues(eq("any,csv,data"));
        verify(mappingIterator, times(2)).hasNext();
        verify(mappingIterator, times(1)).next();
    }

    @Test(expected = Covid19RecordConversionException.class)
    public void shouldThrowErrorConversionGivenIOExceptionWhenFetchingRecords() throws IOException {
        when(objectReader.readValues(anyString())).thenThrow(IOException.class);

        List<Covid19RecordCsvResource> results = mapper.readResourceFromCommaSeparatedString("any,csv,data");

        assertTrue(results.isEmpty());
        verify(basicHeaderConverter, times(1)).getForClass(eq(Covid19RecordCsvResource.class));
        verify(objectReader, times(1)).readValues(eq("any,csv,data"));
        verifyNoInteractions(mappingIterator);
    }

}
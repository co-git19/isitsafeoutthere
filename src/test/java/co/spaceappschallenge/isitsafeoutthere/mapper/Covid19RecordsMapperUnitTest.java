package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
public class Covid19RecordsMapperUnitTest {

    Covid19RecordsMapper mapper;

    @Before
    public void setUp() {
        mapper = new Covid19RecordsMapper();
    }

    @Test
    public void shouldMapAllResourcesToEntities() {
        final Covid19RecordResource resource = new Covid19RecordResourceTestBuilder().build();

        List<Covid19Record> result = mapper.fromResourcesList(singletonList(resource));

        assertResults(result.get(0), resource);
    }

    private void assertResults(Covid19Record targetRecord, Covid19RecordResource originalResource) {
        assertEquals(originalResource.getSource(), targetRecord.getSource());
        assertEquals(originalResource.getCountry(), targetRecord.getCountry());
        assertEquals(originalResource.getProvinceState(), targetRecord.getProvinceState());
        assertEquals(originalResource.getReferenceDate(), targetRecord.getReferenceDate());
        assertEquals(originalResource.getConfirmed(), targetRecord.getConfirmed());
        assertEquals(originalResource.getDeaths(), targetRecord.getDeaths());
        assertEquals(originalResource.getRecovered(), targetRecord.getRecovered());
        assertNull(targetRecord.getId());
        assertNull(targetRecord.getCreatedOn());
        assertNull(targetRecord.getUpdatedOn());
    }

}
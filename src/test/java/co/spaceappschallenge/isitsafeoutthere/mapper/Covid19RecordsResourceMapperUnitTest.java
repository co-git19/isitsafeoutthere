package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.api.uri.JohnsHopkinsUniversityUriGenerator;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.util.ValueUtility;
import co.spaceappschallenge.isitsafeoutthere.view.resource.Covid19RecordCsvResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Covid19RecordsResourceMapperUnitTest {

    @Mock
    private GitHubDateFromFileNameMapper dateFromFileNameMapper;

    @Mock
    private JohnsHopkinsUniversityUriGenerator uriGenerator;

    @Mock
    private ValueUtility valueUtility;

    @InjectMocks
    private Covid19RecordsResourceMapper mapper;

    private Covid19RecordCsvResource nonLegacyRecord;

    private Covid19RecordCsvResource legacyRecord;

    private LocalDate currentDate = now();

    @Before
    public void setUp() {
        when(uriGenerator.getContentsFilePath(anyString())).thenReturn(URI.create("download-uri/05-01-2020.csv"));
        when(dateFromFileNameMapper.fromFileName(anyString())).thenReturn(now());

        when(valueUtility.emptyIfNull(anyString(), any())).thenReturn("dummy-value");
        when(valueUtility.computeIfNull(anyString(), any())).thenReturn("dummy-value");
        when(valueUtility.emptyIfNull(isNull(), any())).thenReturn("");
        when(valueUtility.computeIfNull(isNull(), any())).thenReturn("");

        nonLegacyRecord = new Covid19RecordCsvResource();
        nonLegacyRecord.confirmed = 15;
        nonLegacyRecord.deaths = 2;
        nonLegacyRecord.recovered = 7;
        nonLegacyRecord.currentCountryRegion = "Brazil";
        nonLegacyRecord.currentProvinceState = "Minas Gerais";
        nonLegacyRecord.region = "BH";

        legacyRecord = new Covid19RecordCsvResource();
        legacyRecord.confirmed = 15;
        legacyRecord.deaths = 2;
        legacyRecord.recovered = 7;
        legacyRecord.legacyCountryRegion = "Brazil";
        legacyRecord.legacyProvinceState = "Minas Gerais";
    }

    @Test
    public void shouldMapSingleCurrentResourceWithValidReferenceDate() {
        Covid19RecordResource record = mapper.fromResource("05-01-2020", nonLegacyRecord);

        verify(valueUtility, times(2)).emptyIfNull(isNull(), eq(String.class));
        verify(valueUtility, times(1)).computeIfNull(eq("Brazil"), eq(""));
        verify(valueUtility, times(1)).computeIfNull(eq("Minas Gerais"), eq(""));
        verify(valueUtility, times(1)).emptyIfNull(eq("BH"), eq(String.class));
        assertEquals(15, record.getConfirmed().intValue());
        assertEquals(2, record.getDeaths().intValue());
        assertEquals(7, record.getRecovered().intValue());
        assertEquals(currentDate, record.getReferenceDate());
        assertEquals("download-uri/05-01-2020.csv", record.getSource());
        verify(dateFromFileNameMapper, times(1)).fromFileName(eq("05-01-2020"));
        verify(uriGenerator, times(1)).getContentsFilePath(eq("05-01-2020"));
    }

    @Test
    public void shouldMapSingleLegacyResourceWithValidReferenceDate() {
        Covid19RecordResource record = mapper.fromResource("05-01-2020", legacyRecord);

        verify(valueUtility, times(1)).emptyIfNull(eq("Brazil"), eq(String.class));
        verify(valueUtility, times(1)).emptyIfNull(eq("Minas Gerais"), eq(String.class));
        verify(valueUtility, times(1)).emptyIfNull(isNull(), eq(String.class));
        verify(valueUtility, times(2)).computeIfNull(isNull(), anyString());
        assertEquals(15, record.getConfirmed().intValue());
        assertEquals(2, record.getDeaths().intValue());
        assertEquals(7, record.getRecovered().intValue());
        assertEquals(currentDate, record.getReferenceDate());
        assertEquals("download-uri/05-01-2020.csv", record.getSource());
        verify(dateFromFileNameMapper, times(1)).fromFileName(eq("05-01-2020"));
        verify(uriGenerator, times(1)).getContentsFilePath(eq("05-01-2020"));
    }

    @Test
    public void shouldMapBothLegacyAndNonLegacyRecords() {
        List<Covid19RecordResource> result = mapper.fromResourceList("05-01-2020", asList(legacyRecord,
                nonLegacyRecord));

        assertEquals(2, result.size());
        result.forEach(record -> {
            assertEquals(15, record.getConfirmed().intValue());
            assertEquals(2, record.getDeaths().intValue());
            assertEquals(7, record.getRecovered().intValue());
            assertEquals("download-uri/05-01-2020.csv", record.getSource());
            assertEquals(currentDate, record.getReferenceDate());
        });

        verify(dateFromFileNameMapper, times(1)).fromFileName(eq("05-01-2020"));
        verify(uriGenerator, times(2)).getContentsFilePath(eq("05-01-2020"));
    }

    @Test
    public void shouldMapRecordWithNullStateProvinceToEmptyString() {
        legacyRecord.legacyProvinceState = null;
        nonLegacyRecord.currentProvinceState = null;
        nonLegacyRecord.region = null;

        List<Covid19RecordResource> result = mapper.fromResourceList("05-01-2020", asList(legacyRecord,
                nonLegacyRecord));

        assertEquals(2, result.size());

        result.forEach(
            record -> {
              assertEquals(15, record.getConfirmed().intValue());
              assertEquals(2, record.getDeaths().intValue());
              assertEquals(7, record.getRecovered().intValue());
              assertEquals("download-uri/05-01-2020.csv", record.getSource());
              assertEquals(currentDate, record.getReferenceDate());
            });

        verify(dateFromFileNameMapper, times(1)).fromFileName(eq("05-01-2020"));
        verify(uriGenerator, times(2)).getContentsFilePath(eq("05-01-2020"));
    }

}
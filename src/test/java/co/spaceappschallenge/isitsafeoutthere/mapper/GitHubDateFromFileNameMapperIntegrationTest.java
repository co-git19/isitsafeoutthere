package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.configuration.JohnsHopkinsUniversityDateFormatsConfiguration;
import co.spaceappschallenge.isitsafeoutthere.domain.exception.Covid19RecordConversionParseDateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootProfiledTest
class GitHubDateFromFileNameMapperIntegrationTest {

    @Autowired
    private GitHubDateFromFileNameMapper mapper;

    @Autowired
    private JohnsHopkinsUniversityDateFormatsConfiguration dateFormats;

    @Test
    public void shouldRunMapperAndReturnCorrectDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/y");
        LocalDate expectedDate = LocalDate.parse("05/01/2020", formatter);

        LocalDate result = mapper.fromFileName("05-01-2020.csv");

        assertEquals(expectedDate, result);
    }

    @Test
    public void shouldThrowExceptionWhenFormatIsNotAvailable() {
        Covid19RecordConversionParseDateException resultEx = null;

        try {
            mapper.fromFileName("05-01-2020 13:44.csv");
        } catch (Covid19RecordConversionParseDateException ex) {
            resultEx = ex;
        }

        assertNotNull(resultEx);
        assertEquals("05-01-2020 13:44.csv", resultEx.getFileName());
        assertEquals(dateFormats.getDateFormats(), resultEx.getDateFormats());
    }
}
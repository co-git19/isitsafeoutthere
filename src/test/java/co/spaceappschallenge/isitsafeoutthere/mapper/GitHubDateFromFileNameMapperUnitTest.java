package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.configuration.JohnsHopkinsUniversityDateFormatsConfiguration;
import co.spaceappschallenge.isitsafeoutthere.domain.exception.Covid19RecordConversionParseDateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class GitHubDateFromFileNameMapperUnitTest {

    @Mock
    private JohnsHopkinsUniversityDateFormatsConfiguration dateFormats;

    private GitHubDateFromFileNameMapper mapper;

    @Before
    public void setUp() {
        when(dateFormats.getDateFormats()).thenReturn(newSet("M/d/y", "M-d-y"));

        mapper = new GitHubDateFromFileNameMapper(dateFormats);
    }

    @Test
    public void shouldRunMapperAndReturnCorrectDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/y");
        LocalDate expectedDate = LocalDate.parse("05/01/2020", formatter);

        LocalDate result = mapper.fromFileName("05-01-2020.csv");

        assertEquals(expectedDate, result);
    }

    @Test(expected = Covid19RecordConversionParseDateException.class)
    public void shouldThrowExceptionWhenFormatIsNotAvailable() {

        mapper.fromFileName("rosa.csv");
    }
}
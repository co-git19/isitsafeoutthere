package co.spaceappschallenge.isitsafeoutthere.mapper;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonListResource;
import co.spaceappschallenge.isitsafeoutthere.view.resource.GitHubContentFileJsonResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class GitHubFileResourceMapperUnitTest {

    private GitHubFileResourceMapper resourceMapper;

    @Before
    public void setUp() {
        resourceMapper = new GitHubFileResourceMapper();
    }

    @Test
    public void shouldConvertSingleResourceToDomainEntity() {
        GitHubContentFileJsonResource resource = new GitHubContentFileJsonResource();
        resource.name = "something.csv";
        resource.downloadUrl = "hello-I-am-a-url";

        GitHubContentFile result = resourceMapper.fromResource(resource);

        assertNotNull(result);
        assertEquals("something.csv", result.getName());
        assertEquals("hello-I-am-a-url", result.getDownloadUrl());
    }

    @Test
    public void shouldConvertResourceListToDomainEntities() {
        GitHubContentFileJsonResource resource = new GitHubContentFileJsonResource();
        resource.name = "something-1.csv";
        resource.downloadUrl = "hello-I-am-a-url-1";
        GitHubContentFileJsonListResource resourceList = new GitHubContentFileJsonListResource();
        resourceList.add(resource);
        resource = new GitHubContentFileJsonResource();
        resource.name = "something-2.csv";
        resource.downloadUrl = "hello-I-am-a-url-2";
        resourceList.add(resource);

        List<GitHubContentFile> result = resourceMapper.fromResourceList(resourceList);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.contains(new GitHubContentFile("something-1.csv", "hello-I-am-a-url-1")));
        assertTrue(result.contains(new GitHubContentFile("something-2.csv", "hello-I-am-a-url-2")));
    }

}
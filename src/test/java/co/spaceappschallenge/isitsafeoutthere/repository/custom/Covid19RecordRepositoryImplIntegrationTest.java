package co.spaceappschallenge.isitsafeoutthere.repository.custom;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.repository.Covid19RecordRepository;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParamsTestBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootProfiledTest
public class Covid19RecordRepositoryImplIntegrationTest {

    @Autowired
    private Covid19RecordRepository repository;

    @BeforeEach
    public void setUp() {
        repository.deleteAll();
    }

    @AfterEach
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void shouldFindSingleResult() {
        repository.save(
            new Covid19RecordTestBuilder()
                    .referenceDate(LocalDate.of(2020, 1, 2))
                    .createdOn(LocalDateTime.now())
                    .updatedOn(LocalDateTime.now())
                    .build());
        final Pageable pageable = PageRequest.of(0, 1, Sort.unsorted());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder().build();

        Page<Covid19Record> result = repository.findByQueryParameters(pageable, queryParams);

        assertEquals(1, result.getNumberOfElements());
        assertEquals(1, result.getTotalElements());
    }

    @Test
    public void shouldFindOnlyFilteredValues() {
        repository.saveAll(asList(
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 1))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 2))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 3))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 4))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build()));
        final Pageable pageable = PageRequest.of(0, 2, Sort.unsorted());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .startDate(LocalDate.of(2020, 1, 2))
                .endDate(LocalDate.of(2020, 1, 3))
                .build();

        Page<Covid19Record> result = repository.findByQueryParameters(pageable, queryParams);

        assertEquals(2, result.getNumberOfElements());
        assertEquals(2, result.getTotalElements());
    }

    @Test
    public void shouldFindAndSortRecords() {
        repository.saveAll(asList(
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 1))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Aurora")
                        .referenceDate(LocalDate.of(2020, 1, 2))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("US")
                        .referenceDate(LocalDate.of(2020, 1, 3))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Tibet")
                        .referenceDate(LocalDate.of(2020, 1, 4))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build()));
        final Pageable pageable = PageRequest.of(0, 2, Sort.by("country").descending());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 1, 4))
                .build();

        Page<Covid19Record> result = repository.findByQueryParameters(pageable, queryParams);

        assertEquals(2, result.getNumberOfElements());
        assertEquals(4, result.getTotalElements());
        assertEquals("US", result.getContent().get(0).getCountry());
        assertEquals("Tibet", result.getContent().get(1).getCountry());
    }

    @Test
    public void shouldFindNextPageAndSortRecords() {
        repository.saveAll(asList(
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 1))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Aurora")
                        .referenceDate(LocalDate.of(2020, 1, 2))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("US")
                        .referenceDate(LocalDate.of(2020, 1, 3))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Tibet")
                        .referenceDate(LocalDate.of(2020, 1, 4))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build()));
        final Pageable pageable = PageRequest.of(0, 2, Sort.by("country").descending());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 1, 4))
                .build();

        Page<Covid19Record> result = repository.findByQueryParameters(pageable.next(), queryParams);

        assertEquals(2, result.getNumberOfElements());
        assertEquals(4, result.getTotalElements());
        assertEquals("Brazil", result.getContent().get(0).getCountry());
        assertEquals("Aurora", result.getContent().get(1).getCountry());
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionIfStartDateIsAfterEndDate() {
        RuntimeException re = null;
        final Pageable pageable = PageRequest.of(0, 2, Sort.by("country").descending());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .endDate(LocalDate.of(2020, 1, 1))
                .startDate(LocalDate.of(2020, 1, 4))
                .build();

        try {
            repository.findByQueryParameters(pageable.next(), queryParams);
        } catch (InvalidDataAccessApiUsageException ae) {
            re = ae;
        }

        assertNotNull(re);
        assertTrue(re.getCause() instanceof IllegalArgumentException);
        assertEquals(format("End date (%s) should not be before " +
                "start date (%s)", queryParams.getEndDate(), queryParams.getStartDate()), re.getCause().getMessage());
    }

}
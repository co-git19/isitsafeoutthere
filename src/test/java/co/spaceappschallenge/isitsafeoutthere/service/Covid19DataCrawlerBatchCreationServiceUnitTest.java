package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.util.AsyncResultsResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class Covid19DataCrawlerBatchCreationServiceUnitTest {

    @Mock
    private Covid19ResourceService universityService;

    @Mock
    private Covid19DataCrawlerBatchPreparationService batchPreparator;

    @Mock
    private AsyncResultsResolver asyncResultsResolver;

    private int batchSize = 2;

    private Covid19DataCrawlerBatchCreationService batchCreationService;

    @Before
    public void setUp() throws InterruptedException, ExecutionException {
        batchCreationService = new Covid19DataCrawlerBatchCreationService(universityService, batchPreparator ,
                asyncResultsResolver, batchSize);

        when(asyncResultsResolver.joinResults(anyList())).thenReturn(emptyList());
    }


    @Test
    public void shouldSplitBatchesInChunksOf2Elements() throws InterruptedException, ExecutionException {
        when(universityService.getAllCovid19Records()).thenReturn(newSet(
                new Covid19RecordResourceTestBuilder().build(),
                new Covid19RecordResourceTestBuilder().provinceState("São Paulo").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Rio de Janeiro").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Santa Catarina").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Paraná").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Ceará").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Acre").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Amazonas").build()
        ));

        batchCreationService.createAndRunBatches();

        ArgumentCaptor<List<Covid19RecordResource>> argumentCaptor = forClass(List.class);
        verify(universityService, times(1)).getAllCovid19Records();
        verify(batchPreparator, times(1)).prepareAndRun(eq(1), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(2), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(3), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(4), eq(4), argumentCaptor.capture());
        argumentCaptor.getAllValues().forEach(batch -> assertTrue(batch.size() <= 2));
    }

    @Test
    public void shouldSplitBatchesInChunksOf42lementAndLast1Element() throws InterruptedException, ExecutionException {
        when(universityService.getAllCovid19Records()).thenReturn(newSet(
                new Covid19RecordResourceTestBuilder().build(),
                new Covid19RecordResourceTestBuilder().provinceState("São Paulo").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Rio de Janeiro").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Santa Catarina").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Paraná").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Ceará").build(),
                new Covid19RecordResourceTestBuilder().provinceState("Acre").build()
        ));

        batchCreationService.createAndRunBatches();

        ArgumentCaptor<List<Covid19RecordResource>> argumentCaptor = forClass(List.class);
        verify(universityService, times(1)).getAllCovid19Records();
        verify(batchPreparator, times(1)).prepareAndRun(eq(1), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(2), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(3), eq(4), argumentCaptor.capture());
        verify(batchPreparator, times(1)).prepareAndRun(eq(4), eq(4), argumentCaptor.capture());
        argumentCaptor.getAllValues().forEach(batch -> assertTrue(batch.size() <= 2));
    }

    @Test
    public void shouldNotCreateAnyBatchIfNoResultsAreFound() throws InterruptedException, ExecutionException {
        when(universityService.getAllCovid19Records()).thenReturn(emptySet());

        batchCreationService.createAndRunBatches();

        verify(universityService, times(1)).getAllCovid19Records();
        verifyNoInteractions(batchPreparator);
    }
}
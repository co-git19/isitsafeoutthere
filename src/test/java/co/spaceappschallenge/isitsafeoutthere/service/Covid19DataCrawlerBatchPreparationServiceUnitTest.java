package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordsMapper;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordFieldsExtractor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class Covid19DataCrawlerBatchPreparationServiceUnitTest {

    @Mock
    private Covid19DataCrawlerBatchRunnerService batchRunner;

    @Mock
    private Covid19RecordFieldsExtractor fieldsExtractor;

    @Mock
    private Covid19RecordService service;

    @Mock
    private Covid19RecordsMapper mapper;

    @Mock
    private Covid19RecordSortService sortService;

    @InjectMocks
    private Covid19DataCrawlerBatchPreparationService preparationService;

    private Set<String> countries = newSet();

    private Set<String> provinceStates = newSet();

    private Set<LocalDate> referenceDates = newSet();

    private Page<Covid19Record> matchingRecords = new PageImpl<>(emptyList());

    private List<Covid19Record> batchResult = newArrayList();

    private List<Covid19Record> convertedRecords = newArrayList(new Covid19RecordTestBuilder().build());

    @Before
    public void setUp() {
        when(fieldsExtractor.extractCountries(anyList())).thenReturn(countries);
        when(fieldsExtractor.extractStatesProvinces(anyList())).thenReturn(provinceStates);
        when(fieldsExtractor.extractReferenceDates(anyList())).thenReturn(referenceDates);
        when(service.findByCountriesAndStateProvinciesAndReferenceDates(anySet(), anySet(), anySet(), any()))
                .thenReturn(matchingRecords);
        when(mapper.fromResourcesList(anyList())).thenReturn(convertedRecords);
        when(batchRunner.runBatch(anyList(), anyList())).thenReturn(batchResult);
        when(sortService.sort(eq(convertedRecords))).thenReturn(convertedRecords);
        when(sortService.sort(eq(emptyList()))).thenReturn(emptyList());
    }

    @Test
    public void shouldRun() {
        List<Covid19RecordResource> inputBatch = newArrayList(new Covid19RecordResourceTestBuilder().build());

        preparationService.prepareAndRun(0, 0, inputBatch);

        assertEquals(1, 1);
        verify(fieldsExtractor, times(1)).extractCountries(eq(inputBatch));
        verify(fieldsExtractor, times(1)).extractReferenceDates(eq(inputBatch));
        verify(fieldsExtractor, times(1)).extractStatesProvinces(eq(inputBatch));
        verify(sortService, times(1)).sort(eq(convertedRecords));
        verify(sortService, times(1)).sort(eq(emptyList()));
        verify(mapper, times(1)).fromResourcesList(eq(inputBatch));
        verify(service, times(1)).findByCountriesAndStateProvinciesAndReferenceDates(eq(countries), eq(provinceStates),
                eq(referenceDates), eq(PageRequest.of(0, 1)));
        verify(batchRunner, times(1)).runBatch(eq(batchResult), any());
    }

}
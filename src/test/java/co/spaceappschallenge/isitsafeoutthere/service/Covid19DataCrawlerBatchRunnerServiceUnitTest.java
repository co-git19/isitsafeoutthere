package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.mapper.Covid19RecordUpdater;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Covid19DataCrawlerBatchRunnerServiceUnitTest {

    @Mock
    private Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator;

    @Mock
    private Covid19RecordUpdater updater;

    @InjectMocks
    private Covid19DataCrawlerBatchRunnerService runnerService;

    @Test
    public void shouldNotDoCallsToUpdaterIfThereAreNoRecordsInDatabase() {
        List<Covid19Record> result = runnerService.runBatch(emptyList(),
                singletonList(new Covid19RecordTestBuilder().build()));

        assertEquals(1, result.size());
        verifyNoInteractions(comparator);
        verifyNoInteractions(updater);
    }

    @Test
    public void shouldNotDoUpdatesIfMatchingItemIsNotFound() {
        when(comparator.compare(any(), any())).thenReturn(-1);
        final Covid19Record inputRecord = new Covid19RecordTestBuilder().build();
        final Covid19Record databaseRecord = new Covid19RecordTestBuilder().country("US").build();

        List<Covid19Record> result = runnerService.runBatch(singletonList(databaseRecord), singletonList(inputRecord));

        assertEquals(1, result.size());
        verify(comparator, times(1)).compare(eq(databaseRecord), eq(inputRecord));
        verifyNoInteractions(updater);
    }

    @Test
    public void shouldDoUpdatesIfMatchingItemIsFound() {
        when(comparator.compare(any(), any())).thenReturn(0);
        when(updater.shouldUpdate(any(), any())).thenReturn(true);
        final Covid19Record inputRecord = new Covid19RecordTestBuilder().build();
        final Covid19Record databaseRecord = new Covid19RecordTestBuilder().country("US").build();

        List<Covid19Record> result = runnerService.runBatch(singletonList(databaseRecord), singletonList(inputRecord));

        assertEquals(1, result.size());
        verify(comparator, times(1)).compare(eq(databaseRecord), eq(inputRecord));
        verify(updater, times(1)).doUpdate(eq(databaseRecord), eq(inputRecord));
    }

}
package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.repository.Covid19RecordRepository;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParams;
import co.spaceappschallenge.isitsafeoutthere.view.input.query.Covid19RecordQueryParamsTestBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.internal.util.collections.Sets.newSet;

@SpringBootProfiledTest
public class Covid19RecordServiceIntegrationTest {

    @Autowired
    private Covid19RecordService service;

    @Autowired
    private Covid19RecordRepository repository;

    @BeforeEach
    public void setUp() {
        repository.deleteAll();
    }

    @AfterEach
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void shouldCreateRecord() {
        List<Covid19Record> results = service.save(singletonList(new Covid19RecordTestBuilder().build()));

        assertEquals(1, results.size());
        assertNotNull(results.get(0).getId());
        assertNotNull(results.get(0).getCreatedOn());
        assertNotNull(results.get(0).getUpdatedOn());
        assertEquals(results.get(0).getCreatedOn(), results.get(0).getUpdatedOn());
    }

    @Test
    public void shouldFindRecordById() {
        LocalDate referenceDate = now();
        Long id = service.save(singletonList(new Covid19RecordTestBuilder().region("BH").build())).get(0).getId();

        Optional<Covid19Record> record = service.findOne(id);

        assertTrue(record.isPresent());
        assertEquals("Brazil", record.get().getCountry());
        assertEquals("Minas Gerais", record.get().getProvinceState());
        assertEquals("BH", record.get().getRegion());
        assertEquals(referenceDate, record.get().getReferenceDate());
        assertEquals(0, record.get().getConfirmed());
        assertEquals(0, record.get().getRecovered());
        assertEquals(0, record.get().getDeaths());
        assertEquals(id, record.get().getId());
    }

    @Test
    public void shouldCreateAndFindByPageable() {
        LocalDate referenceDate = now();
        service.save(asList(
                new Covid19RecordTestBuilder().referenceDate(referenceDate).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(1)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(2)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(3)).build()));

        Page<Covid19Record> result = service.findAll(PageRequest.of(0, 1));

        assertEquals(4, result.getTotalElements());
        assertEquals(1, result.getNumberOfElements());
    }

    @Test
    public void shouldFindByCountryStateAndDate() {
        LocalDate referenceDate = now();
        service.save(asList(
                new Covid19RecordTestBuilder().referenceDate(referenceDate).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(1)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(2)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(3)).build()));

        Page<Covid19Record> result =
            service.findByCountriesAndStateProvinciesAndReferenceDates(
                    newSet("Brazil", "United States"),
                    newSet("Minas Gerais", "Alabama"),
                    newSet(referenceDate, referenceDate.plusDays(1), referenceDate.plusDays(2)),
                    PageRequest.of(0, 10));

        assertEquals(3, result.getTotalElements());
        assertEquals(3, result.getNumberOfElements());
    }

    @Test
    public void shouldFindByEmptyState() {
        LocalDate referenceDate = now();
        service.save(asList(
                new Covid19RecordTestBuilder().provinceState("").referenceDate(referenceDate).build(),
                new Covid19RecordTestBuilder().provinceState("").referenceDate(referenceDate.plusDays(1)).build(),
                new Covid19RecordTestBuilder().provinceState("").referenceDate(referenceDate.plusDays(2)).build(),
                new Covid19RecordTestBuilder().provinceState("").referenceDate(referenceDate.plusDays(3)).build()));

        Page<Covid19Record> result =
                service.findByCountriesAndStateProvinciesAndReferenceDates(
                        newSet("Brazil", "United States"),
                        newSet("Minas Gerais", ""),
                        newSet(referenceDate, referenceDate.plusDays(1), referenceDate.plusDays(2)),
                        PageRequest.of(0, 10));

        assertEquals(3, result.getTotalElements());
        assertEquals(3, result.getNumberOfElements());
    }

    @Test
    public void shouldReturnEmptyPageWhenFindByCountryStateAndDate() {
        LocalDate referenceDate = now();

        Page<Covid19Record> result =
                service.findByCountriesAndStateProvinciesAndReferenceDates(
                        newSet("Brazil", "United States"),
                        newSet("", "Minas Gerais", "Alabama"),
                        newSet(referenceDate, referenceDate.plusDays(1), referenceDate.plusDays(2)),
                        PageRequest.of(0, 10));

        assertEquals(0, result.getTotalElements());
        assertEquals(0, result.getNumberOfElements());
    }

    @Test
    public void shouldRemoveEntityAndFindOneReturnsEmpty() {
        Covid19Record singleRecord = service.save(singletonList(new Covid19RecordTestBuilder().build())).get(0);

        service.delete(singleRecord);

        Optional<Covid19Record> result = service.findOne(singleRecord.getId());
        assertFalse(result.isPresent());
    }

    @Test
    public void shouldRemoveEntityByIdAndFindOneReturnsEmpty() {
        Covid19Record singleRecord = service.save(singletonList(new Covid19RecordTestBuilder().build())).get(0);

        service.delete(singleRecord.getId());

        Optional<Covid19Record> result = service.findOne(singleRecord.getId());
        assertFalse(result.isPresent());
    }

    @Test
    public void shouldUpdateEntitiesWithDeathsCounts() {
        LocalDate referenceDate = now();
        List<Covid19Record> created = service.save(asList(
                new Covid19RecordTestBuilder().referenceDate(referenceDate).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(1)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(2)).build(),
                new Covid19RecordTestBuilder().referenceDate(referenceDate.plusDays(3)).build()));

        List<Covid19Record> updated = created
                .stream()
                .peek(record -> record.setDeaths(record.getDeaths() + 2))
                .collect(toList());

        updated = service.save(updated);

        assertEquals(created.size(), updated.size());
        updated.forEach(record -> {
            assertNotEquals(record.getCreatedOn(), record.getUpdatedOn());
            assertEquals(2, record.getDeaths());
        });
    }

    @Test
    public void shouldFindNextPageAndSortRecords() throws Exception {
        repository.saveAll(asList(
                new Covid19RecordTestBuilder()
                        .referenceDate(LocalDate.of(2020, 1, 1))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Aurora")
                        .referenceDate(LocalDate.of(2020, 1, 2))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("US")
                        .referenceDate(LocalDate.of(2020, 1, 3))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build(),
                new Covid19RecordTestBuilder()
                        .country("Tibet")
                        .referenceDate(LocalDate.of(2020, 1, 4))
                        .createdOn(LocalDateTime.now())
                        .updatedOn(LocalDateTime.now())
                        .build()));
        final Pageable pageable = PageRequest.of(0, 2, Sort.by("country").descending());
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .startDate(LocalDate.of(2020, 1, 1))
                .endDate(LocalDate.of(2020, 1, 4))
                .build();

        Page<Covid19Record> result = service.findByQueryParams(pageable.next(), queryParams);

        assertEquals(2, result.getNumberOfElements());
        assertEquals(4, result.getTotalElements());
        assertEquals("Brazil", result.getContent().get(0).getCountry());
        assertEquals("Aurora", result.getContent().get(1).getCountry());
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionIfStartDateIsAfterEndDate() throws Exception {
        RuntimeException re = null;
        final Pageable pageable = PageRequest.of(0, 2);
        final Covid19RecordQueryParams queryParams = new Covid19RecordQueryParamsTestBuilder()
                .country(null)
                .provinceState(null)
                .endDate(LocalDate.of(2020, 1, 1))
                .startDate(LocalDate.of(2020, 1, 4))
                .build();

        try {
            service.findByQueryParams(pageable.next(), queryParams);
        } catch (InvalidDataAccessApiUsageException ae) {
            re = ae;
        }

        assertNotNull(re);
        assertTrue(re.getCause() instanceof IllegalArgumentException);
        assertEquals(format("End date (%s) should not be before " +
                "start date (%s)", queryParams.getEndDate(), queryParams.getStartDate()), re.getCause().getMessage());
    }

}
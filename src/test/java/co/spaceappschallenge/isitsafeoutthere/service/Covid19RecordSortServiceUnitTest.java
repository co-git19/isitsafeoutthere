package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.util.Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Covid19RecordSortServiceUnitTest {

    @Mock
    private Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator;

    @InjectMocks
    private Covid19RecordSortService sortService;

    @Test
    public void shouldCallCompareAndReturnDifferentList() {
        when(comparator.compare(any(), any())).thenReturn(0);
        List<Covid19Record> inputList = asList(
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build(),
                new Covid19RecordTestBuilder().build()
        );

        List<Covid19Record> result = sortService.sort(inputList);

        assertNotSame(inputList, result);
        verify(comparator, times(4)).compare(any(), any());
    }

}
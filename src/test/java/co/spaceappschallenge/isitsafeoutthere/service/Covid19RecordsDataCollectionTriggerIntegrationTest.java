package co.spaceappschallenge.isitsafeoutthere.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class Covid19RecordsDataCollectionTriggerIntegrationTest {

    @MockBean
    private Covid19DataCrawlerBatchCreationService batchCreationService;

    @Autowired
    private Covid19RecordsDataCollectionTrigger trigger;

    @Test
    public void shouldTrigger() throws InterruptedException, ExecutionException  {
        verify(batchCreationService, times(1)).createAndRunBatches();
    }

}
package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.connector.JohnsHopkinsUniversityApiConnector;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class JohnsHopkinsUniversityCollectedDataServiceUnitTest {

    @Mock
    private JohnsHopkinsUniversityApiConnector connector;

    @InjectMocks
    private JohnsHopkinsUniversityCollectedDataService service;

    @Test
    public void shouldCallConnectorToGetRecordsToDownload() {
        Set<GitHubContentFile> inputSet = newSet(
                new GitHubContentFile("1.csv", "url"),
                new GitHubContentFile("2.csv", "url"),
                new GitHubContentFile("3.csv", "url")
        );
        when(connector.getJohnsHopkinsUniversityRepositoryData()).thenReturn(inputSet);

        Set<GitHubContentFile> result = service.getAllCollectedFilesToDownload();

        assertEquals(3, result.size());
        assertTrue(inputSet.containsAll(result));
        verify(connector, times(1)).getJohnsHopkinsUniversityRepositoryData();
    }

    @Test
    public void shouldFilterOnlyNonCsvFiles() {
        Set<GitHubContentFile> inputSet = newSet(
                new GitHubContentFile("1.csv", "url"),
                new GitHubContentFile("2.txt", "url"),
                new GitHubContentFile("3.csv", "url")
        );
        when(connector.getJohnsHopkinsUniversityRepositoryData()).thenReturn(inputSet);

        Set<GitHubContentFile> result = service.getAllCollectedFilesToDownload();

        assertEquals(2, result.size());
        assertTrue(newSet(new GitHubContentFile("1.csv", "url"), new GitHubContentFile("3.csv", "url")).containsAll(result));
        verify(connector, times(1)).getJohnsHopkinsUniversityRepositoryData();
    }


}
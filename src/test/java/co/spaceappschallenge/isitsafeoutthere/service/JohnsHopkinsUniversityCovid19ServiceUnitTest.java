package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.connector.JohnsHopkinsUniversityApiConnector;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class JohnsHopkinsUniversityCovid19ServiceUnitTest {

    @Mock
    private JohnsHopkinsUniversityApiConnector connector;

    @InjectMocks
    private JohnsHopkinsUniversityCovid19Service service;

    @Test
    public void shouldReturnNonEmptyFutureWhenCallSucceeds() throws InterruptedException, ExecutionException {
        Set<Covid19RecordResource> records = newSet(
                new Covid19RecordResourceTestBuilder().country("US").provinceState("Texas").build(),
                new Covid19RecordResourceTestBuilder().build());
        when(connector.getCovidCasesIncidenceAcccordingToJohnsHopkins(anyString(), anyString())).thenReturn(records);

        CompletableFuture<Set<Covid19RecordResource>> resultFuture = service.getAllSamplesByFileNameAndSets("file", "url");

        Set<Covid19RecordResource> result = resultFuture.get();
        assertEquals(2, result.size());
        assertTrue(records.containsAll(result));
        verify(connector, times(1)).getCovidCasesIncidenceAcccordingToJohnsHopkins(eq("file"), eq("url"));
    }

    @Test
    public void shouldReturnEmptySetWhenExceptionsHappen() throws InterruptedException, ExecutionException {
        when(connector.getCovidCasesIncidenceAcccordingToJohnsHopkins(anyString(), anyString()))
                .thenThrow(RuntimeException.class);

        CompletableFuture<Set<Covid19RecordResource>> resultFuture = service.getAllSamplesByFileNameAndSets("file", "url");

        Set<Covid19RecordResource> result = resultFuture.get();
        assertTrue(result.isEmpty());
        verify(connector, times(1)).getCovidCasesIncidenceAcccordingToJohnsHopkins(eq("file"), eq("url"));
    }

}
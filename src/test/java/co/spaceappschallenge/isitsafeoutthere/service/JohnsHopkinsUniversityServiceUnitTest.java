package co.spaceappschallenge.isitsafeoutthere.service;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.GitHubContentFile;
import co.spaceappschallenge.isitsafeoutthere.util.AsyncResultsResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

@RunWith(MockitoJUnitRunner.class)
public class JohnsHopkinsUniversityServiceUnitTest {

    @Mock
    private JohnsHopkinsUniversityCollectedDataService collectedDataService;

    @Mock
    private JohnsHopkinsUniversityCovid19Service covid19Service;

    @Mock
    private AsyncResultsResolver resultsResolver;

    @InjectMocks
    private JohnsHopkinsUniversityService service;

    @Before
    public void setUp() throws InterruptedException, ExecutionException {
        CompletableFuture<Set<Covid19RecordResource>> completableFuture = completedFuture(newSet(
                new Covid19RecordResourceTestBuilder().country("US").provinceState("Alabama").referenceDate(LocalDate.of(2020, 5, 1)).build()
        ));

        when(covid19Service.getAllSamplesByFileNameAndSets(anyString(), anyString()))
                .thenReturn(completableFuture);
        when(resultsResolver.joinResults(anyList())).thenReturn(Collections.singletonList(newSet(
                new Covid19RecordResourceTestBuilder().country("US").source("uri1").provinceState("Alabama").referenceDate(LocalDate.of(2020, 5, 1)).build(),
                new Covid19RecordResourceTestBuilder().country("US").source("uri2").provinceState("Texas").referenceDate(LocalDate.of(2020, 5, 1)).build(),
                new Covid19RecordResourceTestBuilder().country("US").source("uri3").provinceState("California").referenceDate(LocalDate.of(2020, 5, 1)).build()
        )));

        Set<GitHubContentFile> filesToDownload = newSet(
                new GitHubContentFile("name1.csv", "uri1"),
                new GitHubContentFile("name2.csv", "uri2"),
                new GitHubContentFile("name3.csv", "uri3")
        );
        when(collectedDataService.getAllCollectedFilesToDownload()).thenReturn(filesToDownload);
    }

    @Test
    public void shouldCombineAllCovid19RecordsInOneCall()  throws InterruptedException, ExecutionException {
        Set<Covid19RecordResource> result = service.getAllCovid19Records();

        assertEquals(3, result.size());
        verify(collectedDataService, times(1)).getAllCollectedFilesToDownload();
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name1.csv"), eq("uri1"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name2.csv"), eq("uri2"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name3.csv"), eq("uri3"));
        verify(resultsResolver, times(1)).joinResults(anyList());
    }

    @Test
    public void shouldReturnEmptyListInCaseInterruptedExceptionIsThrown()  throws InterruptedException,
            ExecutionException {

        when(resultsResolver.joinResults(anyList())).thenThrow(InterruptedException.class);

        Set<Covid19RecordResource> result = service.getAllCovid19Records();

        assertTrue(result.isEmpty());
        verify(collectedDataService, times(1)).getAllCollectedFilesToDownload();
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name1.csv"), eq("uri1"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name2.csv"), eq("uri2"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name3.csv"), eq("uri3"));
        verify(resultsResolver, times(1)).joinResults(anyList());
    }

    @Test
    public void shouldReturnEmptyListInCaseExecutionExceptionIsThrown()  throws InterruptedException,
            ExecutionException {

        when(resultsResolver.joinResults(anyList())).thenThrow(ExecutionException.class);

        Set<Covid19RecordResource> result = service.getAllCovid19Records();

        assertTrue(result.isEmpty());
        verify(collectedDataService, times(1)).getAllCollectedFilesToDownload();
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name1.csv"), eq("uri1"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name2.csv"), eq("uri2"));
        verify(covid19Service, times(1)).getAllSamplesByFileNameAndSets(eq("name3.csv"), eq("uri3"));
        verify(resultsResolver, times(1)).joinResults(anyList());
    }
}
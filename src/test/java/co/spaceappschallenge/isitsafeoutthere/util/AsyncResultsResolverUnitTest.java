package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class AsyncResultsResolverUnitTest {

    private AsyncResultsResolver resultsResolver;

    @Before
    public void setUp() {
        resultsResolver = new AsyncResultsResolver();
    }

    @Test
    public void shouldJoinAllResultsInSingleList() throws InterruptedException, ExecutionException {
        List<Set<Covid19RecordResource>> results =
            resultsResolver.joinResults(asList(completedFuture(emptySet()), completedFuture(emptySet())));

        assertEquals(2, results.size());
        results.forEach(result -> assertTrue(result.isEmpty()));
    }

    @Test
    public void shouldReturnEmptyListGivenEmptyListAsInput() throws InterruptedException, ExecutionException {
        List<Set<Covid19RecordResource>> results =
                resultsResolver.joinResults(emptyList());

        assertTrue(results.isEmpty());
    }

}
package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;
import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19RecordTestBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootProfiledTest
public class Covid19RecordDateAndCountryAndProvinceStateAndRegionComparatorIntegrationTest {

    private static final LocalDate REFERENCE_DATE = now();

    @Autowired
    public Covid19RecordDateAndCountryAndProvinceStateAndRegionComparator comparator;

    @Test
    public void shouldReturnZeroForEqualRecords() {
        Covid19Record first = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();
        Covid19Record second = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertEquals(0, result);
    }

    @Test
    public void shouldReturnGreaterThan0IfFirstRecordHappensAfterThanSecond() {
        Covid19Record first = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE.plusDays(1)).build();
        Covid19Record second = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertTrue(result > 0);
    }

    @Test
    public void shouldReturnGreaterThan0IfDateIsEqualsButFirstCountryIsAfterSecondCountry() {
        Covid19Record first = new Covid19RecordTestBuilder()
                .country("United States").referenceDate(REFERENCE_DATE).build();
        Covid19Record second = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertTrue(result > 0);
    }

    @Test
    public void shouldReturnGreaterThan0IfDateIsEqualsButFirstCountryIsAfterSecondCountryIndependentOfState() {
        Covid19Record first = new Covid19RecordTestBuilder()
                .country("United States").provinceState("Alabama").referenceDate(REFERENCE_DATE).build();
        Covid19Record second = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertTrue(result > 0);
    }

    @Test
    public void shouldReturnGreaterThan0IfDateAndCountryAreEqualsButFirstStateIsAfterSecondState() {
        Covid19Record first = new Covid19RecordTestBuilder()
                .provinceState("Rio de Janeiro").referenceDate(REFERENCE_DATE).build();
        Covid19Record second = new Covid19RecordTestBuilder().referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertTrue(result > 0);
    }

    @Test
    public void shouldReturnGreaterThan0IfDateAndCountryAndProvinceStateAreEqualsButFirstRegionIsAfterSecondRegion() {
        Covid19Record first = new Covid19RecordTestBuilder().region("BH").referenceDate(REFERENCE_DATE).build();
        Covid19Record second = new Covid19RecordTestBuilder().region("Abaeté").referenceDate(REFERENCE_DATE).build();

        int result = comparator.compare(first, second);

        assertTrue(result > 0);
    }

    @Test
    public void shouldFindIndexByBinarySearch() {
        Covid19Record first = new Covid19RecordTestBuilder()
                .provinceState("Rio de Janeiro")
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record second = new Covid19RecordTestBuilder()
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record third = new Covid19RecordTestBuilder()
                .country("United States")
                .provinceState("Alabama")
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record fourth = new Covid19RecordTestBuilder()
                .country("United States")
                .provinceState("Alabama")
                .referenceDate(REFERENCE_DATE.plusDays(1))
                .build();
        List<Covid19Record> list = asList(second, first, third, fourth);

        int index = Collections.binarySearch(list, new Covid19RecordTestBuilder()
                .country("United States")
                .provinceState("Alabama")
                .build(), comparator);

        assertEquals(2, index);
    }

    @Test
    public void shouldNotFindIndexByBinarySearch() {
        Covid19Record first = new Covid19RecordTestBuilder()
                .provinceState("Rio de Janeiro")
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record second = new Covid19RecordTestBuilder()
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record third = new Covid19RecordTestBuilder()
                .country("United States")
                .provinceState("Alabama")
                .referenceDate(REFERENCE_DATE)
                .build();
        Covid19Record fourth = new Covid19RecordTestBuilder()
                .country("United States")
                .provinceState("Alabama")
                .referenceDate(REFERENCE_DATE.plusDays(1))
                .build();
        List<Covid19Record> list = asList(second, first, third, fourth);

        int index = Collections.binarySearch(list, new Covid19RecordTestBuilder().country("Mexico").build(),
                comparator);

        assertTrue(index < 0);
    }
}
package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResource;
import co.spaceappschallenge.isitsafeoutthere.domain.resource.Covid19RecordResourceTestBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class Covid19RecordFieldsExtractorUnitTest {

    private static final LocalDate REFERENCE_DATE = now();

    private Covid19RecordFieldsExtractor extractor;

    private List<Covid19RecordResource> inputResource;

    @Before
    public void setUp() {
        extractor = new Covid19RecordFieldsExtractor();
        inputResource = asList(
                new Covid19RecordResourceTestBuilder().referenceDate(REFERENCE_DATE).build(),
                new Covid19RecordResourceTestBuilder().referenceDate(REFERENCE_DATE).provinceState("São Paulo").build(),
                new Covid19RecordResourceTestBuilder().referenceDate(REFERENCE_DATE).country("United States").provinceState("Alabama").build(),
                new Covid19RecordResourceTestBuilder().referenceDate(REFERENCE_DATE).country("United States").provinceState("California").build(),
                new Covid19RecordResourceTestBuilder().referenceDate(REFERENCE_DATE).country("Chile").provinceState("Andes").build()
        );
    }

    @Test
    public void shouldExtractCountryNames() {
        Set<String> countries = extractor.extractCountries(inputResource);

        assertEquals(3, countries.size());
    }

    @Test
    public void shouldExtractStateProvincesNames() {
        Set<String> stateProvinces = extractor.extractStatesProvinces(inputResource);

        assertEquals(5, stateProvinces.size());
    }

    @Test
    public void shouldExtractOnlyReferenceDates() {
        Set<LocalDate> dates = extractor.extractReferenceDates(inputResource);

        assertEquals(1, dates.size());;
    }

    @Test
    public void shouldExtractNullField() {
        List<Covid19RecordResource> records = new ArrayList<>(inputResource);
        records.add(new Covid19RecordResourceTestBuilder().provinceState(null).build());

        Set<String> statesProvincesWithNull = extractor.extractStatesProvinces(records);

        assertEquals(6, statesProvincesWithNull.size());
        assertTrue(statesProvincesWithNull.contains(null));
    }

}
package co.spaceappschallenge.isitsafeoutthere.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PageableRepositoryUtilsUnitTest {

    @Mock
    private Root<Object> root;

    @InjectMocks
    private PageableRepositoryUtils repositoryUtils;

    @Test
    public void shouldReturnListOfOrders() {
        Pageable page = PageRequest.of(0, 10, Sort.by("dummy-attribute").ascending());

        List<Order> orders = repositoryUtils.toOrders(page, root);

        verify(root, times(1)).get(eq("dummy-attribute"));
        assertEquals(1, orders.size());
        assertTrue(orders.get(0).isAscending());
    }

}
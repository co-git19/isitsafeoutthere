package co.spaceappschallenge.isitsafeoutthere.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class Utf8StringQueryParamResolverUnitTest {

    private Utf8StringQueryParamResolver paramResolver;

    @Before
    public void setUp() {
        paramResolver = new Utf8StringQueryParamResolver();
    }

    @Test
    public void shouldEncodeSpacesCorrectly() {
        String result = paramResolver.doEncode("Minas Gerais");

        assertEquals("Minas+Gerais", result);
    }

    @Test
    public void shouldDecodeSpacesCorrectly() {
        String result = paramResolver.doDecode("Minas+Gerais");

        assertEquals("Minas Gerais", result);
    }

}
package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.SpringBootProfiledTest;
import co.spaceappschallenge.isitsafeoutthere.domain.exception.EmptyElementMapperNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootProfiledTest
public class ValueUtilityIntegrationTest {

    @Autowired
    private ValueUtility valueUtility;

    @Test
    public void shouldReturnEmptyStringGivenNull() {
        String result = valueUtility.emptyIfNull(null, String.class);

        assertEquals("", result);
    }

    @Test
    public void shouldReturnIfNullStringValueGivenNull() {
        String value = null;

        String result = valueUtility.computeIfNull(value, "if-null");

        assertEquals("if-null", result);
    }

    @Test
    public void shouldReturnParamGivenNonNull() {
        String result = valueUtility.emptyIfNull("param", String.class);

        assertEquals("param", result);
    }

    @Test
    public void shouldReturnParamIfNotNullGivenIfNull() {
        String result = valueUtility.computeIfNull("param", "non-null");

        assertEquals(result, "param");
    }

    @Test
    public void shouldReturnEmptyStringGivenNullInteger() {
        Integer result = valueUtility.emptyIfNull(null, Integer.class);

        assertEquals(0, result);
    }

    @Test
    public void shouldReturnIfNullStringValueGivenNullInteger() {
        Integer value = null;

        Integer result = valueUtility.computeIfNull(value, 2);

        assertEquals(2, result);
    }

    @Test
    public void shouldReturnParamGivenNonNullInteger() {
        Integer result = valueUtility.emptyIfNull(1, Integer.class);

        assertEquals(1, result);
    }

    @Test
    public void shouldReturnParamIfNotNullGivenIfNullInteger() {
        Integer result = valueUtility.computeIfNull(1, 2);

        assertEquals(result, 1);
    }

    @Test
    public void shouldThrowIllegalStateException() {
        RuntimeException re = null;

        try {
            valueUtility.emptyIfNull(null, Object.class);
        } catch (EmptyElementMapperNotFoundException nfe) {
            re = nfe;
        }

        assertNotNull(re);
        assertEquals(format("Could not find an appropriate computer for class %s.", Object.class),
                re.getMessage());
    }

}
package co.spaceappschallenge.isitsafeoutthere.util;

import co.spaceappschallenge.isitsafeoutthere.util.computer.EmptyElementMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValueUtilityUnitTest {

    @Mock
    private EmptyElementMapper elementMapper;

    private ValueUtility valueUtility;

    @Before
    public void setUp() {
        valueUtility = new ValueUtility(singletonList(elementMapper));
    }

    @Test
    public void shouldReturnEmptyElement() {
        final Object emptyObject = new Object();
        when(elementMapper.applies(any())).thenReturn(true);
        when(elementMapper.getEmptyElement()).thenReturn(emptyObject);

        Object result = valueUtility.emptyIfNull(null, Object.class);

        assertSame(emptyObject, result);
        verify(elementMapper, times(1)).applies(eq(Object.class));
        verify(elementMapper, times(1)).getEmptyElement();
    }

    @Test
    public void shouldReturnNonEmptyElement() {
        Object result = valueUtility.emptyIfNull("empty-string", Object.class);

        assertEquals("empty-string", result);
        verifyNoInteractions(elementMapper);
    }

    @Test
    public void shouldReturnAdditionalParameterIfProvidenIsNull() {
        Object result = valueUtility.computeIfNull(null, "additional-value");

        assertEquals("additional-value", result);
        verifyNoInteractions(elementMapper);
    }

}
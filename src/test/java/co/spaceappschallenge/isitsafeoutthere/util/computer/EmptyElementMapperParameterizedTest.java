package co.spaceappschallenge.isitsafeoutthere.util.computer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(Parameterized.class)
public class EmptyElementMapperParameterizedTest<T> {

    private EmptyElementMapper emptyElementMapper;

    private Class<T> value;

    private Class<?> notApplicableValue;

    private T emptyValue;

    public EmptyElementMapperParameterizedTest(EmptyElementMapper emptyElementMapper, Class<T> value,
                                               Class<?> notApplicableValue, T emptyValue) {
        this.emptyElementMapper = emptyElementMapper;
        this.value = value;
        this.notApplicableValue = notApplicableValue;
        this.emptyValue = emptyValue;
    }

    @Test
    public void shouldApplyToSpecifiedValue() {
        boolean shouldApply = emptyElementMapper.applies(value);

        assertTrue(shouldApply);
    }

    @Test
    public void shouldNotApplyToSpecifiedValue() {
        boolean shouldApply = emptyElementMapper.applies(notApplicableValue);

        assertFalse(shouldApply);
    }

    @Test
    public void shouldReturnEmptyValue() {
        T result = emptyElementMapper.getEmptyElement();

        assertEquals(emptyValue, result);
    }

    @Parameterized.Parameters
    public static Object[][] parameters() {
        return new Object[][] {
                new Object[] { new StringEmptyElementMapper(), String.class, Integer.class, "" },
                new Object[] { new IntegerEmptyElementMapper(), Integer.class, String.class, 0 }
        };
    }

}
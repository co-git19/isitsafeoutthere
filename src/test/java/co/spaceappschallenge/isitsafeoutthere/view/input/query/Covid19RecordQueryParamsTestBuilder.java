package co.spaceappschallenge.isitsafeoutthere.view.input.query;

import java.time.LocalDate;

public class Covid19RecordQueryParamsTestBuilder {

    private final Covid19RecordQueryParams queryParams;

    public Covid19RecordQueryParamsTestBuilder() {
        queryParams = new Covid19RecordQueryParams("Brazil", "Minas Gerais", LocalDate.of(2020, 1, 1),
                LocalDate.of(2020, 12, 31));
    }

    public Covid19RecordQueryParamsTestBuilder country(String country) {
        queryParams.setCountry(country);
        return this;
    }

    public Covid19RecordQueryParamsTestBuilder provinceState(String provinceState) {
        queryParams.setProvinceState(provinceState);
        return this;
    }

    public Covid19RecordQueryParamsTestBuilder startDate(LocalDate startDate) {
        queryParams.setStartDate(startDate);
        return this;
    }

    public Covid19RecordQueryParamsTestBuilder endDate(LocalDate endDate) {
        queryParams.setEndDate(endDate);
        return this;
    }

    public Covid19RecordQueryParams build() {
        return queryParams;
    }

}
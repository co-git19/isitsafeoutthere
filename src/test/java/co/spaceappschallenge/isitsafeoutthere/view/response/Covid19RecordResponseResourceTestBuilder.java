package co.spaceappschallenge.isitsafeoutthere.view.response;

import co.spaceappschallenge.isitsafeoutthere.domain.entity.Covid19Record;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Covid19RecordResponseResourceTestBuilder {

    private final Covid19RecordResponseResource responseResource;

    public Covid19RecordResponseResourceTestBuilder() {
        responseResource = new Covid19RecordResponseResource(1L,"Brazil", "Minas Gerais", null, LocalDate.now(), 2, 2
                , 2, "source", LocalDateTime.now(), LocalDateTime.now());
    }

    public Covid19RecordResponseResourceTestBuilder(Long id, String country, String provinceState, String region,
                                                    LocalDate referenceDate, Integer confirmed, Integer recovered,
                                                    Integer deaths, String source, LocalDateTime createdOn,
                                                    LocalDateTime updatedOn) {
        responseResource = new Covid19RecordResponseResource(id, country, provinceState, region, referenceDate,
                confirmed, recovered, deaths, source, createdOn, updatedOn);
    }

    public Covid19RecordResponseResourceTestBuilder(Covid19Record entity) {
        responseResource = new Covid19RecordResponseResource(entity);
    }

    public Covid19RecordResponseResourceTestBuilder id(Long id) {
        responseResource.setId(id);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder country(String country) {
        responseResource.setCountry(country);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder provinceState(String provinceState) {
        responseResource.setProvinceState(provinceState);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder referenceDate(LocalDate referenceDate) {
        responseResource.setReferenceDate(referenceDate);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder confirmed(Integer confirmed) {
        responseResource.setConfirmed(confirmed);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder recovered(Integer recovered) {
        responseResource.setRecovered(recovered);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder deaths(Integer deaths) {
        responseResource.setDeaths(deaths);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder source(String source) {
        responseResource.setSource(source);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder createdOn(LocalDateTime createdOn) {
        responseResource.setCreatedOn(createdOn);
        return this;
    }

    public Covid19RecordResponseResourceTestBuilder updatedOn(LocalDateTime updatedOn) {
        responseResource.setUpdatedOn(updatedOn);
        return this;
    }

    public Covid19RecordResponseResource build() {
        return responseResource;
    }

}